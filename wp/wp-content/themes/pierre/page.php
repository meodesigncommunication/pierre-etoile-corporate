<?php
get_header(); ?>

<div class="content-page-wrap container">
    <?php while(have_posts()):the_post();?>
    <div class="about-head">
        <h2><?php the_title();?></h2>
        <p><?php the_field("subtitle");?></p>
    </div>
    <div class="image-wrap">
        <?php  if (has_post_thumbnail()){the_post_thumbnail("full");}?>
    </div>
    <div class="row content-page-article">
        <div class="default-sidebar-wrap">
            <?php get_sidebar();?>
        </div>
        <article class="span8 article-basic">
            <?php the_content();?>
        </article>
    </div>
        <?php endwhile;?>
</div>

<?php
get_footer();
