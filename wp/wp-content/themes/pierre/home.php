<?php
/*
* Template Name: Events
* */
get_header();?>

<div class="container">
    <div class="about-head">
        <h2><?php echo get_the_title(23)?></h2>
        <p><?php echo get_field("subtitle",23)?></p>
    </div>
</div>
<div class="media-filter-wrapper">
    <div class="container filter">
        <h4><?php _e("[:en]Filter[:fr]Filtrer");?><span id="plus-media-menu"></span><span id="selected-filter"><?php _e("[:en]view all[:fr]tout voir");?></span></h4>
        <ul class="unstyled inline filter-option-list">
            <li class="<?php if((!isset($_REQUEST["future"]))and(!isset($_REQUEST["past"]))){echo "chosen";}?>" data-filter="*">
                <a href="<?php echo get_permalink(23)?>" class="filter-link"><?php _e("[:en]view all[:fr]tout voir");?></a>
                <span class="filter-param">1111</span>
            </li>

            <li class="<?php if(isset($_REQUEST["future"])){echo "chosen";}?>" data-filter=".future">
                <a href="<?php echo pierre_add_param_to_link(get_permalink(23), "future");?>" class="filter-link"><?php _e("[:en]future[:fr]à venir");?></a>
                <span class="filter-param">1111</span>

            </li>
            <li class="<?php if(isset($_REQUEST["past"])){echo "chosen";}?>" data-filter=".past">
                <a href="<?php echo pierre_add_param_to_link(get_permalink(23), "past");?>" class="filter-link"><?php _e("[:en]past[:fr]réalisé");?></a>

                <span class="filter-param ">1111</span>
            </li>
        </ul>
    </div>
</div>
<div class="about-us-wrapper">
    <?php
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
    $args = array(
        'posts_per_page'   => 99999,
        'post_status'      => 'publish',
        'orderby'          => 'post_date',
        'order'            => 'DESC' ,
//        'paged' => $paged
    );



    if((isset($_REQUEST["future"])) or (isset($_REQUEST["past"]))){
        $today = date('Y-m-d',time());
//        $args["orderby"] = "meta_value";
//        $args["order"] = "ASC";
        $args["meta_key"] = "event_date";
        $args["meta_value"] = $today;
        if(isset($_REQUEST["future"])){
            $args["meta_compare"] = ">=";
        }
        if(isset($_REQUEST["past"])){
            $args["meta_compare"] = "<";
        }
    }

    $wp_query = new WP_Query($args);

//    $posts = get_posts($args);
//        if(count($posts)){
            if(have_posts()){
                $today = date('Y-m-d',time());
            ?>
    <div id="container" class="events-post-list masonry container">
        <?php while(have_posts()){ the_post();global $post; $item = $post;
        $event_date = get_field("event_date",$item->ID);
        if($today >= $event_date){ $class_filter = "past";}else{$class_filter = "future";}
        ?>
        <div class="column item <?php echo $class_filter;?>">
            <?php if(has_post_thumbnail($item->ID)){?>
            <?php echo get_the_post_thumbnail($item->ID, "pierreetoile-standard");?>
            <?php }else{?>
            <img src="<?php echo get_template_directory_uri();?>/images/img_to_come.png" alt="image">
            <?php }?>
            <h4 class="type-title"><?php the_field("promotion", $item->ID);?></h4>
            <h3 class="media-title"><?php echo get_the_title($item->ID);?></h3>
            <p class="publish-date"><?php _e("[:fr]Publié le[:en]Published on");?> <span><?php echo pierre_date_rep(mb_strtolower(get_the_date("l d F Y")))?></span></p>
            <?php echo apply_filters('the_content', get_post_field('post_content', $item->ID)); ?>
        </div>
            <?php }?>

    </div>
    <div class="pagination-wrap">
                <?php //get_template_part("block","pagination");?>
            <?php }?>
    </div>
</div>


<?php get_footer();?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<!--<script type='text/javascript' src='http://isotope.metafizzy.co/beta/isotope.pkgd.js'></script>-->
<script type='text/javascript' src='<?php echo get_template_directory_uri();?>/js/jquery.isotope.js'></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        var $container = jQuery('#container');
        $container.isotope({
            itemSelector: '.item',
            layoutMode: 'fitRows'
        });



        jQuery('.filter-option-list li').click(function(){
            jQuery("#plus-media-menu").removeClass("reversed");
            jQuery(".filter-option-list li").removeClass("opened");
            jQuery('.filter-option-list li').removeClass("chosen");

            var title = jQuery(this).children("a").html();
            jQuery("#selected-filter").html(title);

            jQuery(this).addClass("chosen");
            var selector = jQuery(this).attr('data-filter');
            $container.isotope({ filter: selector });
            return false;
        });
    });
</script>

        
<script src="<?php echo get_template_directory_uri();?>/js/jquery.masonry.min.js"></script>
<script type="text/javascript">
//    $(document).ready(function() {
//        $('#container').masonry({
//            itemSelector: '.item',
//            singleMode: true
//                }).imagesLoaded(function() {
//                    $('#container').masonry('reload');
//                });
//    });
</script>