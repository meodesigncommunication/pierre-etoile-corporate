<?php
/*
* Template Name: Calculation
* */
get_header();?>

<div class="container">
    <div class="about-head">
        <?php while(have_posts()):the_post();?>
        <h2><?php the_title();?></h2>
        <p><?php the_field("subtitle");?></p>
        <?php endwhile;?>
    </div>
</div>
<div class="media-filter-wrapper">
    <div class="container calc-head">
        <ul class="unstyled inline calc-step-list nonmobile-calc-list">
            <li class="active etape1_link">
                <h3><?php _e("[:fr]étape[:en]stage");?> 1</h3>
                <p><?php _e("[:fr]Privé et investisseur[:en]Private investor");?></p>
            </li>
            <li class="etape2_link">
                <h3><?php _e("[:fr]étape[:en]stage");?> 2</h3>
                <p><?php _e("[:fr]Privé et investisseur[:en]Private investor");?></p>
            </li>
            <li class="etape3_link">
                <h3><?php _e("[:fr]étape[:en]stage");?> 3</h3>
                <p><?php _e("[:fr]investisseur[:en]investor");?></p>
            </li>
        </ul>
        <ul class="unstyled inline calc-step-list mobile-calc-list">
            <li class=" etapes_link possible">
                <h3><?php _e("[:fr]étape[:en]stage");?> 1</h3>
                <p><?php _e("[:fr]Privé et investisseur[:en]Private investor");?></p>
                <span id="plus-media-menu"></span>
            </li>
            <li class="active etape1_link exp-li possible">
                <h3><?php _e("[:fr]étape[:en]stage");?> 1</h3>
                <p><?php _e("[:fr]Privé et investisseur[:en]Private investor");?></p>
            </li>
            <li class="etape2_link exp-li">
                <h3><?php _e("[:fr]étape[:en]stage");?> 2</h3>
                <p><?php _e("[:fr]Privé et investisseur[:en]Private investor");?></p>
            </li>
            <li class="etape3_link exp-li">
                <h3><?php _e("[:fr]étape[:en]stage");?> 3</h3>
                <p><?php _e("[:fr]investisseur[:en]investor");?></p>
            </li>
        </ul>
    </div>
</div>
    <?php $currency = get_field("currency","option"); ?>
<div class="media-us-wrapper">
    <div class="page-wrap container step1-container calc-container">
        <div class="row step1">
            <div class="span4">
                <p class="description"><?php the_field("first_stage_text","option");?></p>
            </div>
            <div class="span12 ">
                <p class="description"><?php the_field("first_stage_text","option");?></p>
            </div>
        </div>
        <div class="row step1">
            <div class="span6">
                <form>
                    <ul class="calc-list">
                        <li class="input-group">
                            <label for="apartment-price"><?php _e("[:fr]Prix de l’appartement[:en]Price of apartment");?>*</label>
                            <input type="text" name="apartment-price" id="apartment-price" />
                            <span  class="input-group-addon"> <?php echo $currency; ?></span>
                        </li>
                        <li>
                            <label for="parking-price"><?php _e("[:fr]Prix de la place de parc[:en]Price of parking space");?></label>
                            <input type="text" name="parking-price" id="parking-price" />
                            <span> <?php echo $currency; ?></span>
                        </li>
                        <li>
                            <label for="cellar-price"><?php _e("[:fr]Prix de la cave[:en]Price cellar");?></label>
                            <input type="text" name="cellar-price" id="cellar-price" />
                            <span> <?php echo $currency; ?></span>
                        </li>
                    </ul>
                </form>
            </div>
            <div class="span6">
                <form>
                    <ul class="calc-list">
                        <li class="equity-input">
                            <label for="available-equity"><?php _e("[:fr]Montant de vos fonds propres disponible[:en]Amount of your available equity");?>*</label>
                            <input type="text" name="available-equity" id="available-equity" />
                            <span> <?php echo $currency; ?></span>
                        </li>
                        <li>
                            <label for="available-equity"><?php _e("[:fr]Taux actuel[:en]Current rate");?>*</label>
                            <input type="text" name="current-rate" id="current-rate"  value="2.2"/>
                            <span> %</span>
                        </li>
                        <li class="attention-taux"><?php _e("[:fr]Ce taux de 2.2% est indicatif. Les taux hypothécaires varient selon le marché et la situation de chacun. Merci de vérifier que ce taux hypothécaire s’applique à votre situation auprès de votre établissement bancaire.[:en]This rate is only 2.2%. Mortgage rates vary by market and individual circumstances. Please verify that mortgage rate applies to your situation with your bank.");?>
                        </li>
                    </ul>
                    <div class="errorcalc" id="errorcalc_1" style="display: none;"><?php _e("[:fr]S'il vous plaît remplir tous les champs requis[:en]Please fill in all required fields");?></div>
                    <button class="calc"  id="calc_step_1"><?php _e("[:fr]calculer[:en]calculate");?></button>
                </form>
            </div>
        </div>
    </div>
</div>



<div class="media-us-wrapper">
    <div class="page-wrap container  calc-container">
        <div class="row step2">
            <span class="print-icon">&nbsp; </span>
            <span class="download-icon">&nbsp; </span>
            <div class="span6">
                <div class="step2-block" >
                    <h3><?php _e("[:fr]Coût d’aquisition[:en]Acquisition cost");?></h3>
                    <ul class="calculator-block-list" >
                        <li >
                            <p class="first-column"><?php _e("[:fr]Prix d’achat de l’appartement[:en]Purchase price of the apartment");?></p>
                            <p class="last-column"><span> <?php echo $currency; ?>&nbsp;</span><span id="prix-appartment-out">450'000</span></p>
                        </li>
                        <li >
                            <p class="first-column"><?php _e("[:fr]Prix de la place de parc[:en]Price of parking space");?></p>
                            <p class="last-column"><span> <?php echo $currency; ?>&nbsp;</span><span id="prix-parc-out">450'000</span></p>
                        </li>
                        <li >
                            <p class="first-column"><?php _e("[:fr]Prix de la cave[:en]Price cellar");?></p>
                            <p class="last-column"><span> <?php echo $currency; ?>&nbsp;</span><span id="prix-cave-out">450'000</span></p>
                        </li>
                        <li >
                            <p class="first-column"><?php _e("[:fr]Frais de notaire, droit de mutation et cédule hypothécaire[:en]Notary fees, transfer duty and mortgage note");?></p>
                            <p class="last-column"><span> <?php echo $currency; ?>&nbsp;</span><span id="prix-notaire-out">450'000</span></p>
                        </li>
                        <li >
                            <p class="first-column summery"><?php _e("[:fr]Coût total de l’aquisition[:en]Total cost of acquisition");?></p>
                            <p class="last-column summery"><span> <?php echo $currency; ?>&nbsp;</span><span id="prix-total-out">450'000</span></p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="span6">
                <div class="step2-block">
                    <h3 ><?php _e("[:fr]Fonds[:en]Funds");?></h3>
                    <ul class="calculator-block-list">
                        <li >
                            <p class="first-column"><?php _e("[:fr]Fonds propres[:en]Equity capital");?></p>
                            <p class="last-column"><span> <?php echo $currency; ?>&nbsp;</span><span id="fonds-propres">450'000</span></p>
                        </li>
                        <li >
                            <p class="first-column"><?php _e("[:fr]Prêt hypothécaire[:en]Mortgage loan");?></p>
                            <p class="last-column"><span> <?php echo $currency; ?>&nbsp;</span><span id="fonds-hypothecaire">450'000</span></p>
                        </li>
                        <li >
                            <p class="first-column summery"><?php _e("[:fr]Total des fonds[:en]Total Funds");?></p>
                            <p class="last-column summery"><span> <?php echo $currency; ?>&nbsp;</span><span id="fonds-total">450'000</span></p>
                        </li>
                    </ul>
                </div>
                <div class="step2-block" >
                    <h3><?php _e("[:fr]Charges[:en]Charges");?></h3>
                    <ul class="calculator-block-list">
                        <li >
                            <p class="first-column"><?php _e("[:fr]Charges hypothécaires annuelles[:en]Mortgage Charges");?><span class="infos_in"><?php _e("[:fr]taux actuel à <span id='current-rate-span'>2.2</span>%[:en]at current annual rate <span id='current-rate-span'>2.2</span>%");?></span></p>
                            <p class="last-column"><span> <?php echo $currency; ?>&nbsp;</span><span id="estimatedPurchasePricePerYear">450'000</span></p>
                        </li>
                        <li>
                            <p class="first-column"><?php _e("[:fr]Charge de copropriété[:en]Condo fees");?><span class="infos_in"><?php _e("[:fr]1% du prix d’achat la 1<sup><small>ère</small></sup> année[:en]1% of the purchase price the first year");?></span></p>
                            <p class="last-column"><span> <?php echo $currency; ?>&nbsp;</span><span id="amortisationPerYear">450'000</span></p>
                        </li>
                        <li >
                            <p class="first-column summery"><?php _e("[:fr]TOTAL DES CHARGES ANNUELLES[:en]TOTAL ANNUAL EXPENSES");?></p>
                            <p class="last-column summery"><span> <?php echo $currency; ?>&nbsp;</span><span id="charges-annuelles">450'000</span></p>
                        </li>
                        <li >
                            <p class="first-column summery"><?php _e("[:fr]TOTAL DES CHARGES MENSUELLES[:en]TOTAL MONTHLY EXPENSES");?></p>
                            <p class="last-column summery"><span> <?php echo $currency; ?>&nbsp;</span><span id="charges-mensuelles">450'000</span></p>
                        </li>
                    </ul>
                </div>
                <p class="vous-allez"><?php _e("[:fr]Vous allez louer votre bien?[:en]Are you going to rent your property?");?></p>
                <button class="calc" id="calc_step_2"><?php _e("[:fr]calculer le rendement locatif[:en]calculate the rental yield");?></button>
            </div>
        </div>
    </div>
</div>

<div class="container step3-wrapper">
    <div class="row">
        <div class="span6 no-print">
            <p class="step3_intro"><?php _e("[:fr]Indiquer le loyer mensuel estimé, afin de calculer le rendement de votre bien.[:en]Indicate the estimated monthly rent to calculate the rendement your property.");?></p>
            <form>
                <ul class="calc-list">
                    <li>
                        <label for="apartment-rent-estimated"><?php _e("[:fr]Loyer estimé de l’appartement[:en]Estimated rent of the apartment");?>*</label>
                        <input type="text" name="apartment-rent-estimated" id="apartment-rent-estimated" class="grey" />
                        <span> <?php echo $currency; ?>&nbsp;</span>
                    </li>
                    <li>
                        <label for="parking-rent-estimated"><?php _e("[:fr]Loyer estimé de la place de parc[:en]Estimated rent of the car parking");?></label>
                        <input type="text" name="parking-rent-estimated" id="parking-rent-estimated" class="grey" />
                        <span> <?php echo $currency; ?>&nbsp;</span>
                    </li>
<!--                    <li>-->
<!--                        <label for="parking-rent-estimated">--><?php //_e("[:fr]Loyer estimé de la cave[:en]Estimated rent of the cellar");?><!--</label>-->
<!--                        <input type="text" name="parking-rent-estimated" id="cellar-rent-estimated" class="grey" />-->
<!--                        <span>--><?php //echo $currency; ?> <!--</span>-->
<!--                    </li>-->
                    <li>
                        <label for="standard-expenses-estimated"><?php _e("[:fr]Charges forfaitaires estimées[:en]Standard expenses estimated");?>*</label>
                        <input type="text" name="standard-expenses-estimated" id="standard-expenses-estimated" class="grey" />
                        <span> <?php echo $currency; ?>&nbsp;</span>
                    </li>

                </ul>
                <div class="errorcalc" id="errorcalc_3" style="display: none;"><?php _e("[:fr]S'il vous plaît remplir tous les champs requis [:en]Please fill in all required fields");?></div>
                <button class="calc"  id="calc_step_3"><?php _e("[:fr]calculer le rendement[:en]calculate");?></button>
            </form>
        </div>
        <div class="span6">
            <p class="step3_intro hidden_intro" style="opacity: 0"><?php _e("[:fr]Indiquer le loyer mensuel estimé, afin de calculer le rendement de votre bien.[:en]Indicate the estimated monthly rent to calculate the rendement your property.");?></p>

            <div class="step4" id="step4">
                <div class="grey-bg">
                    <h3><?php _e("[:fr]Rendement[:en]Yield");?></h3>
                    <ul class="calculator-block-list">
                        <li>
                            <p class="first-column"><?php _e("[:fr]Recette mensuel[:en]Monthly revenue");?></p>
                            <p class="last-column"><span> <?php echo $currency; ?>&nbsp;</span><span id="recette_mensuel">450'000</span></p>
                        </li>
                        <li>
                            <p class="first-column"><?php _e("[:fr]Recette annuel[:en]Annual revenue");?></p>
                            <p class="last-column"><span> <?php echo $currency; ?>&nbsp;</span><span id="recette_annuel">450'000</span></p>
                        </li>
                        <li class="gain">
                            <p class="first-column"><?php _e("[:fr]GAIN[:en]PROFIT");?>
                                <span class="special_for_pdf" style="display: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <span class="special"><?php _e("[:fr]loyer estimé - charges annuelles[:en]estimated rent - annual charges");?></span></p>
                            <p class="last-column"><span> <?php echo $currency; ?>&nbsp;</span><span id="profit">450'000</span></p>
                        </li>
                    </ul>
                </div>
                <ul class="calculator-block-list invested">
                    <li>
                        <p class="first-column"><?php _e("[:fr]Fonds propres investis[:en]Invested equity capital");?></p>
                        <p class="last-column">
                            <span class="special_for_pdf" style="display: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            <span id="fonds_propres_ivestis">450'000</span><span> <?php //echo $currency; ?> %</span></p>
                    </li>
                    <li>
                        <p class="first-column"><?php _e("[:fr]Fonds propres + frais d’achat investis[:en]Own funds + purchase costs invested");?></p>
                        <p class="last-column"><span>
                                <span class="special_for_pdf" style="display: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <span id="fonds_plus_frais">450'000</span><?php //echo $currency; ?> %</span></p>
                    </li>
                </ul>
            </div>
        </div>

    </div>
</div>


    <script type="text/javascript">

    function isNumber(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }


        function generatePdf(source){
            var html = "<div class=\"row step2\">"+jQuery(".row.step2").html()+"</div>";
            var is_rendement_visible = jQuery("#step4").is(":visible");
            var html2 = "";
            if(is_rendement_visible){
            html2 = "<div id=\"step4\">"+jQuery("#step4").html()+"</div>";
            }
//            alert(html);
//            html = jQuery.toJSON(html);
            var data = {
                action:'pierre_genetate_pdf',
                html:html,
                html2:html2,
                currency: "<?php echo $currency;?>"
            };
            source.addClass("loading");
            jQuery.ajax({
                type: "POST",
                url:  "<?php echo admin_url('admin-ajax.php'); ?>",
                data: data,
                success: function(msg){
                    source.removeClass("loading");
                    msg = msg.substring(0, msg.length - 1);
                    window.open(msg, "_blank");
//                    alert(msg);
                },
                error: function(msg){
                    source.removeClass("loading");
                    console.log(msg);
                }
            });
        }



        function addCommas(nStr, to_print)
        {

            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + to_print + '$2');
            }
            return x1 + x2;
        }
        function recalculateAll(print){

            var contentOut = "";

            var apartmentPriceContainer = jQuery("#apartment-price");
            var parkingPriceContainer = jQuery("#parking-price");
            var cellarPriceContainer = jQuery("#cellar-price");
            var availableEnquiteContainer = jQuery("#available-equity");

            var chargesAnnuellesContainer = jQuery("#charges-annuelles");
            var chargesMensuellesContainer = jQuery("#charges-mensuelles");
            var estimatedPurchasePricePerYearContainer = jQuery("#estimatedPurchasePricePerYear");
            var amortisationPerYearContainer = jQuery("#amortisationPerYear");

            var prixAppartmentOutContainer = jQuery("#prix-appartment-out");
            var prixParcOutContainer = jQuery("#prix-parc-out");
            var prixCaveOutContainer = jQuery("#prix-cave-out");
            var prixNotaireOutContainer = jQuery("#prix-notaire-out");
            var prixTotalOutContainer = jQuery("#prix-total-out");

            var fondsPropresContainer = jQuery("#fonds-propres");
            var fondsHypoContainer = jQuery("#fonds-hypothecaire");
            var fondsTotalContainer = jQuery("#fonds-total");






            var feesTaxNotePercent = 5;
            var buyerDepositPercent = 20;


            var annualMortgageContainer = jQuery("#current-rate");
            var annualMortgageVal = (annualMortgageContainer.val())?annualMortgageContainer.val():"2.2";
            var annualMortgageChargesRate = parseFloat(annualMortgageVal);
            jQuery("#current-rate-span").html(annualMortgageChargesRate);


            var estimatedPercent = 1;

            var apartmentVal = (apartmentPriceContainer.val())?apartmentPriceContainer.val():"0";
            var parkingVal = (parkingPriceContainer.val())?parkingPriceContainer.val():"0";
            var cellarVal = (cellarPriceContainer.val())?cellarPriceContainer.val():"0";

            var availableEnquiteVal = (availableEnquiteContainer.val())?availableEnquiteContainer.val():"0";

            var apartmentPrice = parseInt(apartmentVal);
            var parkingPrice = parseInt(parkingVal);
            var cellarPrice = parseInt(cellarVal);
            var availableEnquite = parseInt(availableEnquiteVal);

            var purchasePrice = apartmentPrice + parkingPrice + cellarPrice;
            var notaryFeesTaxNote = Math.floor((purchasePrice*feesTaxNotePercent)/100);
            var totalPurchaseCost = purchasePrice + notaryFeesTaxNote;

            var to_what = (print == "0")?'\'':'';

            prixAppartmentOutContainer.html(addCommas(apartmentPrice, to_what));
            prixParcOutContainer.html(addCommas(parkingPrice, to_what));
            prixCaveOutContainer.html(addCommas(cellarPrice, to_what));
            prixNotaireOutContainer.html(addCommas(notaryFeesTaxNote, to_what));
            prixTotalOutContainer.html(addCommas(totalPurchaseCost, to_what));


            contentOut += "purchasePrice: "+purchasePrice+"\n\n";
            contentOut += "notaryFeesTaxNote: "+notaryFeesTaxNote+"\n\n";
            contentOut += "totalPurchaseCost: "+totalPurchaseCost+"\n\n";

//            var buyerDeposit = Math.floor((purchasePrice*buyerDepositPercent)/100);
            var buyerDeposit = availableEnquite;

            var amountOfMortgage = purchasePrice - buyerDeposit;
            contentOut += "amountOfMortgage: "+amountOfMortgage+"\n\n";

            var annualMortgageCharges = Math.floor((amountOfMortgage*annualMortgageChargesRate)/100);
            contentOut += "annualMortgageCharges: "+annualMortgageCharges+"\n\n";


            fondsPropresContainer.html(addCommas(buyerDeposit, to_what));
            fondsHypoContainer.html(addCommas(amountOfMortgage, to_what));
            fondsTotalContainer.html(addCommas(totalPurchaseCost, to_what));

            var estimatedPurchasePricePerYear = Math.floor((purchasePrice*estimatedPercent)/100);
            estimatedPurchasePricePerYearContainer.html(addCommas(estimatedPurchasePricePerYear, to_what));

            var amortisationPerYear = Math.floor((amountOfMortgage*estimatedPercent)/100);
            amortisationPerYearContainer.html(addCommas(amortisationPerYear, to_what));

            var totalFinanceCostsPerYear = annualMortgageCharges + estimatedPurchasePricePerYear + amortisationPerYear;
            chargesAnnuellesContainer.html(addCommas(totalFinanceCostsPerYear, to_what));

            var amortisationPerMonth = Math.floor(totalFinanceCostsPerYear/12);
            chargesMensuellesContainer.html(addCommas(amortisationPerMonth, to_what));





            var apartmentRentContainer = jQuery("#apartment-rent-estimated");
            var parkingRentContainer = jQuery("#parking-rent-estimated");
//            var cellarRentContainer = jQuery("#cellar-rent-estimated");
            var standardExpensesContainer = jQuery("#standard-expenses-estimated");

            var apartmentRentVal = (apartmentRentContainer.val())?apartmentRentContainer.val():"0";
            var parkingRentVal = (parkingRentContainer.val())?parkingRentContainer.val():"0";
            var standardExpensesVal = (standardExpensesContainer.val())?standardExpensesContainer.val():"0";

            var apartmentRentValCount = parseInt(apartmentRentVal);
            var parkingRentValCount = parseInt(parkingRentVal);
            var standardExpensesValCount = parseInt(standardExpensesVal);

            var monthlyRentIncome = apartmentRentValCount + parkingRentValCount + standardExpensesValCount;
            var yearlyRentIncome = monthlyRentIncome * 12;

            var totalAnnualCosts = annualMortgageCharges + amortisationPerYear;

            var PROFIT = yearlyRentIncome - totalAnnualCosts;

            var buyerFundsInvested = Math.floor((PROFIT/buyerDeposit)*100);
            var buyerFundsInvestedPlusTaxes = Math.floor((PROFIT/(buyerDeposit+notaryFeesTaxNote))*100);


            var recette_mensuelContainer = jQuery("#recette_mensuel");
            var recette_annuelContainer = jQuery("#recette_annuel");
            var profitContainer = jQuery("#profit");
            var fonds_propres_ivestisContainer = jQuery("#fonds_propres_ivestis");
            var fonds_plus_fraisContainer = jQuery("#fonds_plus_frais");

            recette_mensuelContainer.html(addCommas(monthlyRentIncome, to_what));
            recette_annuelContainer.html(addCommas(yearlyRentIncome, to_what));
            profitContainer.html(addCommas(PROFIT, to_what));
            fonds_propres_ivestisContainer.html(addCommas(buyerFundsInvested, to_what));
            fonds_plus_fraisContainer.html(addCommas(buyerFundsInvestedPlusTaxes, to_what));


        }
        jQuery(document).ready(function(){
            jQuery("#calc_step_1").click(function(){

                jQuery(".etape1_link, .etape2_link, .etape3_link").removeClass("opened");
                jQuery(this).removeClass("reversed");

                var val_1 = jQuery("#apartment-price").val();
                var val_2 = jQuery("#available-equity").val();
                var val_3 = jQuery("#current-rate").val();

                if((val_1 == "") || (val_2 == "") || (val_3 == "") || (!isNumber(val_1)) || (!isNumber(val_2)) || (!isNumber(val_3))){
                    jQuery("#errorcalc_1").slideDown("fast");
                }
                else{
                recalculateAll("0");
                jQuery("#errorcalc_1").slideUp();
                jQuery(".step1-container").slideUp("fast");
                jQuery(".step2").slideDown("fast");
                jQuery(".etape1_link").removeClass("active").addClass("passed");
                jQuery(".etape2_link").addClass("active");

                    var title = jQuery(".etape2_link").children("h3").html();
                    var subtitle = jQuery(".etape2_link").children("p").html();
                    jQuery(".etapes_link").children("h3").html(title);
                    jQuery(".etapes_link").children("p").html(subtitle);
                }
                return false;
            });

            jQuery("#calc_step_2").click(function(){
//                alert("lol");
                jQuery(".etape1_link, .etape2_link, .etape3_link").removeClass("opened");
                jQuery(this).removeClass("reversed");

                jQuery(".step3-wrapper").slideDown("fast");
                jQuery(".etape2_link").removeClass("active").addClass("passed");
                jQuery(".etape2_link").addClass("possible");
                jQuery(".etape3_link").addClass("active");

                var title = jQuery(".etape3_link").children("h3").html();
                var subtitle = jQuery(".etape3_link").children("p").html();
                jQuery(".etapes_link").children("h3").html(title);
                jQuery(".etapes_link").children("p").html(subtitle);

                return false;
            });

            jQuery("#calc_step_3").click(function(){

                jQuery(".etape1_link, .etape2_link, .etape3_link").removeClass("opened");
                jQuery(this).removeClass("reversed");

                var val_1 = jQuery("#apartment-rent-estimated").val();
                var val_2 = jQuery("#standard-expenses-estimated").val();

                if((val_1 == "") || (val_2 == "") || (!isNumber(val_1)) || (!isNumber(val_2))){
                    jQuery("#errorcalc_3").slideDown("fast");
                }
                else{
                jQuery("#errorcalc_3").slideUp("fast");
                recalculateAll("0");
                jQuery(".step4").slideDown("fast");
                }
                return false;
            });


            jQuery(".etape1_link").click(function(){

                jQuery(".etape1_link, .etape2_link, .etape3_link").removeClass("opened");
                jQuery("#plus-media-menu").removeClass("reversed");
                jQuery(".etape2_link").removeClass("possible");

                var title = jQuery(".etape1_link").children("h3").html();
                var subtitle = jQuery(".etape1_link").children("p").html();
                jQuery(".etapes_link").children("h3").html(title);
                jQuery(".etapes_link").children("p").html(subtitle);


                recalculateAll("0");
                jQuery(".step1-container").slideDown("fast");
                jQuery(".step2").slideUp("fast");
                jQuery(".step3-wrapper").slideUp("fast");
                jQuery(".step4").slideUp("fast");
                jQuery(".etape1_link").addClass("active");
                jQuery(".etape2_link").removeClass("active");
                jQuery(".etape3_link").removeClass("active");
                return false;
            });



            jQuery(".etape2_link").click(function(){
                if(jQuery(this).hasClass("possible")){

                jQuery(".etape1_link, .etape2_link, .etape3_link").removeClass("opened");
                jQuery("#plus-media-menu").removeClass("reversed");

                var title = jQuery(".etape2_link").children("h3").html();
                var subtitle = jQuery(".etape2_link").children("p").html();
                jQuery(".etapes_link").children("h3").html(title);
                jQuery(".etapes_link").children("p").html(subtitle);

                jQuery(".step3-wrapper").slideUp("fast");
                jQuery(".step4").slideUp("fast");
                jQuery(".etape2_link").addClass("active");
                jQuery(".etape1_link").removeClass("active");
                jQuery(".etape3_link").removeClass("active");
                }
                return false;
            });

            jQuery(".print-icon").click(function(){
//                recalculateAll("1");
//                generatePdf(jQuery(this));
                window.print();
                return false;
            });
            jQuery(".download-icon").click(function(){
//                recalculateAll("1");
                generatePdf(jQuery(this));
                return false;
            });




        });
    </script>

<?php get_footer();?>