<?php

define("PE_PAGE_ID_NEWSLETTER", 19);



/**
 * Set up the content width value based on the theme's design.
 *
 * @see pierreetoile_content_width()
 *
 * @since Twenty Fourteen 1.0
 */
if (!isset($content_width)) {
    $content_width = 474;
}

if (!function_exists('pierreetoile_setup')) :
    /**
     * Twenty Fourteen setup.
     *
     * Set up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support post thumbnails.
     *
     * @since Twenty Fourteen 1.0
     */
    function pierreetoile_setup()
    {

        /*
       * Make Twenty Fourteen available for translation.
       *
       * Translations can be added to the /languages/ directory.
       * If you're building a theme based on Twenty Fourteen, use a find and
       * replace to change 'pierreetoile' to the name of your theme in all
       * template files.
       */
        load_theme_textdomain('pierreetoile', get_template_directory() . '/languages');

        // This theme styles the visual editor to resemble the theme style.
        add_editor_style(array('css/editor-style.css', pierreetoile_font_url()));

        // Add RSS feed links to <head> for posts and comments.
        add_theme_support('automatic-feed-links');

        // Enable support for Post Thumbnails, and declare two sizes.
        add_theme_support('post-thumbnails');
        set_post_thumbnail_size(672, 372, true);
        add_image_size('pierreetoile-full-width', 2076, 1152, true);
        add_image_size('pierreetoile-standard', 620, 460, true);
        add_image_size('pierreetoile-portrait', 460, 560, true);

        // This theme uses wp_nav_menu() in two locations.
        register_nav_menus(array(
            'primary' => __('Top primary menu', 'pierreetoile'),
            'secondary' => __('Secondary menu in left sidebar', 'pierreetoile'),
        ));

        /*
       * Switch default core markup for search form, comment form, and comments
       * to output valid HTML5.
       */
        add_theme_support('html5', array(
            'search-form', 'comment-form', 'comment-list',
        ));

        /*
       * Enable support for Post Formats.
       * See http://codex.wordpress.org/Post_Formats
       */
        add_theme_support('post-formats', array(
            'aside', 'image', 'video', 'audio', 'quote', 'link', 'gallery',
        ));

        // This theme allows users to set a custom background.
        add_theme_support('custom-background', apply_filters('pierreetoile_custom_background_args', array(
            'default-color' => 'f5f5f5',
        )));

        // Add support for featured content.
        add_theme_support('featured-content', array(
            'featured_content_filter' => 'pierreetoile_get_featured_posts',
            'max_posts' => 6,
        ));

        // This theme uses its own gallery styles.
        add_filter('use_default_gallery_style', '__return_false');
    }
endif; // pierreetoile_setup
add_action('after_setup_theme', 'pierreetoile_setup');

/**
 * Adjust content_width value for image attachment template.
 *
 * @since Twenty Fourteen 1.0
 *
 * @return void
 */
function pierreetoile_content_width()
{
    if (is_attachment() && wp_attachment_is_image()) {
        $GLOBALS['content_width'] = 810;
    }
}

add_action('template_redirect', 'pierreetoile_content_width');

/**
 * Getter function for Featured Content Plugin.
 *
 * @since Twenty Fourteen 1.0
 *
 * @return array An array of WP_Post objects.
 */
function pierreetoile_get_featured_posts()
{
    /**
     * Filter the featured posts to return in Twenty Fourteen.
     *
     * @since Twenty Fourteen 1.0
     *
     * @param array|bool $posts Array of featured posts, otherwise false.
     */
    return apply_filters('pierreetoile_get_featured_posts', array());
}

/**
 * A helper conditional function that returns a boolean value.
 *
 * @since Twenty Fourteen 1.0
 *
 * @return bool Whether there are featured posts.
 */
function pierreetoile_has_featured_posts()
{
    return !is_paged() && (bool)pierreetoile_get_featured_posts();
}

/**
 * Register three Twenty Fourteen widget areas.
 *
 * @since Twenty Fourteen 1.0
 *
 * @return void
 */
function pierreetoile_widgets_init()
{

    register_sidebar(array(
        'name' => __('Primary Sidebar', 'pierreetoile'),
        'id' => 'sidebar-1',
        'description' => __('Main sidebar that appears on the left.', 'pierreetoile'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h1 class="widget-title">',
        'after_title' => '</h1>',
    ));
}

add_action('widgets_init', 'pierreetoile_widgets_init');

/**
 * Register Lato Google font for Twenty Fourteen.
 *
 * @since Twenty Fourteen 1.0
 *
 * @return string
 */
function pierreetoile_font_url()
{
    $font_url = '';
    /*
      * Translators: If there are characters in your language that are not supported
      * by Lato, translate this to 'off'. Do not translate into your own language.
      */
    if ('off' !== _x('on', 'Lato font: on or off', 'pierreetoile')) {
        $font_url = add_query_arg('family', urlencode('Lato:300,400,700,900,300italic,400italic,700italic'), "//fonts.googleapis.com/css");
    }

    return $font_url;
}

/**
 * Enqueue scripts and styles for the front end.
 *
 * @since Twenty Fourteen 1.0
 *
 * @return void
 */
function pierreetoile_scripts()
{
    // Add Lato font, used in the main stylesheet.
    wp_enqueue_style('pierreetoile-lato', pierreetoile_font_url(), array(), null);

    // Load our main stylesheet.
    wp_enqueue_style('pierreetoile-style', get_stylesheet_uri(), array('genericons'));

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

    if (is_singular() && wp_attachment_is_image()) {
        wp_enqueue_script('pierreetoile-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array('jquery'), '20130402');
    }

    if (is_active_sidebar('sidebar-3')) {
        wp_enqueue_script('jquery-masonry');
    }

    if (is_front_page() && 'slider' == get_theme_mod('featured_content_layout')) {
        wp_enqueue_script('pierreetoile-slider', get_template_directory_uri() . '/js/slider.js', array('jquery'), '20131205', true);
        wp_localize_script('pierreetoile-slider', 'featuredSliderDefaults', array(
            'prevText' => __('Previous', 'pierreetoile'),
            'nextText' => __('Next', 'pierreetoile')
        ));
    }

    wp_enqueue_script('pierreetoile-script', get_template_directory_uri() . '/js/functions.js', array('jquery'), '20131209', true);
}

add_action('wp_enqueue_scripts', 'pierreetoile_scripts');

/**
 * Enqueue Google fonts style to admin screen for custom header display.
 *
 * @since Twenty Fourteen 1.0
 *
 * @return void
 */
function pierreetoile_admin_fonts()
{
    wp_enqueue_style('pierreetoile-lato', pierreetoile_font_url(), array(), null);
}

add_action('admin_print_scripts-appearance_page_custom-header', 'pierreetoile_admin_fonts');

if (!function_exists('pierreetoile_the_attached_image')) :
    /**
     * Print the attached image with a link to the next attached image.
     *
     * @since Twenty Fourteen 1.0
     *
     * @return void
     */
    function pierreetoile_the_attached_image()
    {
        $post = get_post();
        /**
         * Filter the default Twenty Fourteen attachment size.
         *
         * @since Twenty Fourteen 1.0
         *
         * @param array $dimensions {
         *     An array of height and width dimensions.
         *
         * @type int $height Height of the image in pixels. Default 810.
         * @type int $width  Width of the image in pixels. Default 810.
         * }
         */
        $attachment_size = apply_filters('pierreetoile_attachment_size', array(810, 810));
        $next_attachment_url = wp_get_attachment_url();

        /*
       * Grab the IDs of all the image attachments in a gallery so we can get the URL
       * of the next adjacent image in a gallery, or the first image (if we're
       * looking at the last image in a gallery), or, in a gallery of one, just the
       * link to that image file.
       */
        $attachment_ids = get_posts(array(
            'post_parent' => $post->post_parent,
            'fields' => 'ids',
            'numberposts' => -1,
            'post_status' => 'inherit',
            'post_type' => 'attachment',
            'post_mime_type' => 'image',
            'order' => 'ASC',
            'orderby' => 'menu_order ID',
        ));

        // If there is more than 1 attachment in a gallery...
        if (count($attachment_ids) > 1) {
            foreach ($attachment_ids as $attachment_id) {
                if ($attachment_id == $post->ID) {
                    $next_id = current($attachment_ids);
                    break;
                }
            }

            // get the URL of the next image attachment...
            if ($next_id) {
                $next_attachment_url = get_attachment_link($next_id);
            } // or get the URL of the first image attachment.
            else {
                $next_attachment_url = get_attachment_link(array_shift($attachment_ids));
            }
        }

        printf('<a href="%1$s" rel="attachment">%2$s</a>',
            esc_url($next_attachment_url),
            wp_get_attachment_image($post->ID, $attachment_size)
        );
    }
endif;

if (!function_exists('pierreetoile_list_authors')) :
    /**
     * Print a list of all site contributors who published at least one post.
     *
     * @since Twenty Fourteen 1.0
     *
     * @return void
     */
    function pierreetoile_list_authors()
    {
        $contributor_ids = get_users(array(
            'fields' => 'ID',
            'orderby' => 'post_count',
            'order' => 'DESC',
            'who' => 'authors',
        ));

        foreach ($contributor_ids as $contributor_id) :
            $post_count = count_user_posts($contributor_id);

            // Move on if user has not published a post (yet).
            if (!$post_count) {
                continue;
            }
            ?>

        <div class="contributor">
            <div class="contributor-info">
                <div class="contributor-avatar"><?php echo get_avatar($contributor_id, 132); ?></div>
                <div class="contributor-summary">
                    <h2 class="contributor-name"><?php echo get_the_author_meta('display_name', $contributor_id); ?></h2>

                    <p class="contributor-bio">
                        <?php echo get_the_author_meta('description', $contributor_id); ?>
                    </p>
                    <a class="contributor-posts-link"
                       href="<?php echo esc_url(get_author_posts_url($contributor_id)); ?>">
                        <?php printf(_n('%d Article', '%d Articles', $post_count, 'pierreetoile'), $post_count); ?>
                    </a>
                </div>
                <!-- .contributor-summary -->
            </div>
            <!-- .contributor-info -->
        </div><!-- .contributor -->

        <?php
        endforeach;
    }
endif;

/**
 * Extend the default WordPress body classes.
 *
 * Adds body classes to denote:
 * 1. Single or multiple authors.
 * 2. Presence of header image.
 * 3. Index views.
 * 4. Full-width content layout.
 * 5. Presence of footer widgets.
 * 6. Single views.
 * 7. Featured content layout.
 *
 * @since Twenty Fourteen 1.0
 *
 * @param array $classes A list of existing body class values.
 * @return array The filtered body class list.
 */
function pierreetoile_body_classes($classes)
{
    if (is_multi_author()) {
        $classes[] = 'group-blog';
    }

    if (get_header_image()) {
        $classes[] = 'header-image';
    } else {
        $classes[] = 'masthead-fixed';
    }

    if (is_archive() || is_search() || is_home()) {
        $classes[] = 'list-view';
    }

    if ((!is_active_sidebar('sidebar-2'))
        || is_page_template('page-templates/full-width.php')
        || is_page_template('page-templates/contributors.php')
        || is_attachment()
    ) {
        $classes[] = 'full-width';
    }

    if (is_active_sidebar('sidebar-3')) {
        $classes[] = 'footer-widgets';
    }

    if (is_singular() && !is_front_page()) {
        $classes[] = 'singular';
    }

    if (is_front_page() && 'slider' == get_theme_mod('featured_content_layout')) {
        $classes[] = 'slider';
    } elseif (is_front_page()) {
        $classes[] = 'grid';
    }

    return $classes;
}

add_filter('body_class', 'pierreetoile_body_classes');

/**
 * Extend the default WordPress post classes.
 *
 * Adds a post class to denote:
 * Non-password protected page with a post thumbnail.
 *
 * @since Twenty Fourteen 1.0
 *
 * @param array $classes A list of existing post class values.
 * @return array The filtered post class list.
 */
function pierreetoile_post_classes($classes)
{
    if (!post_password_required() && has_post_thumbnail()) {
        $classes[] = 'has-post-thumbnail';
    }

    return $classes;
}

add_filter('post_class', 'pierreetoile_post_classes');

/**
 * Create a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *
 * @since Twenty Fourteen 1.0
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function pierreetoile_wp_title($title, $sep)
{
    global $paged, $page;

    if (is_feed()) {
        return $title;
    }

    // Add the site name.
    $title .= get_bloginfo('name');

    // Add the site description for the home/front page.
    $site_description = get_bloginfo('description', 'display');
    if ($site_description && (is_home() || is_front_page())) {
        $title = "$title $sep $site_description";
    }

    // Add a page number if necessary.
    if ($paged >= 2 || $page >= 2) {
        $title = "$title $sep " . sprintf(__('Page %s', 'pierreetoile'), max($paged, $page));
    }

    return $title;
}

add_filter('wp_title', 'pierreetoile_wp_title', 10, 2);


/*#########################################################################################*/
/*#########################################################################################*/
/*#########################################################################################*/
/*#########################################################################################*/

add_action("login_head", "pierreetoile_login_head");
function pierreetoile_login_head()
{
    echo "
	<style>
	body.login #login h1 {
		background: #ccc;
margin-left: 0px;
box-shadow: 2px 0px 2px #ccc;
border-radius: 5px;
	}
	body.login #login h1 a {
		background: url('" . get_bloginfo('template_url') . "/images/logo.png') no-repeat scroll center top transparent;
height: 56px;
width: 319px;
margin-bottom: 20px;
padding: 0;
	}
	</style>
	";
}

add_filter('login_headerurl', create_function(false, "return '" . get_bloginfo("url") . "';"));
add_filter('login_headertitle', create_function(false, "return '" . get_bloginfo("name") . "';"));

function pierre_language_selector() {
	global $q_config;
	if (empty($q_config)) {
		return; // qtranslate disabled
	}
	echo '<div class="change-language-menu">';
	echo   '<ul class="list-unstyled">';

	$class = 'first';
	foreach(qtrans_getSortedLanguages() as $language) {
		$class .= ($language == $q_config['language']) ? ' current-item' : '';
        $page = get_page_by_path('');
        $p = qts_get_slug( $page->ID,$language);
		echo '<li' . ( empty($class) ? '' : ' class="' . $class . '"' ) . '><a href="' . $p.'?lang='.$language . '">' . strtoupper($language) . '</a></li>';
		$class = '';
	}
	echo   '</ul>';
	echo '</div>';
}


function pierre_add_param_to_link($link, $params)
{
    if (!is_array($params)) {
        $params = array($params);
    }
    if (count($params)) {
        foreach ($params as $param) {
            if (strpos($link, "?")) {
                $link .= "&" . $param;
            } else {
                $link .= "?" . $param;
            }
        }
    }

    return $link;
}


function generate_results_pdf()
{
    error_reporting(E_ERROR);
    global $wpdb;
    $html = $_POST["html"];
    $html2 = $_POST["html2"];
    $currency = $_POST["currency"];
    $html = preg_replace("/\\\'/", ".", $html);
    $html2 = preg_replace("/\\\'/", ".", $html2);

    $html = preg_replace("/" . $currency . "/", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . $currency, $html);
    $html = preg_replace("/calculer le rendement locatif/", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" , $html);
    $html = preg_replace("/calculate the rental yield/", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" , $html);
    $html = preg_replace("/Vous allez louer votre bien\?/", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" , $html);
    $html = preg_replace("/Are you going to rent your property\?/", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" , $html);

    $html2 = preg_replace("/" . $currency . "/", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . $currency, $html2);
    $html2 = preg_replace("/<span id=\"fonds_/", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id=\"fonds_" , $html2);

    include(get_template_directory() . "/tools/MPDF57/mpdf.php");


    $mpdf = new mPDF(get_bloginfo('charset'), 'A4', '8', '', 10, 10, 7, 7, 10, 10);

    $mpdf->charset_in = get_bloginfo('charset');


    $style_link = get_template_directory() . '/css/style.css';
    $stylesheet = "<style>" . file_get_contents($style_link) . "</style>";
    $mpdf->WriteHTML($stylesheet, 1);


    $style_link = get_template_directory() . '/css/bootstrap/css/bootstrap.css';
    $stylesheet = "<style>" . file_get_contents($style_link) . "</style>";
    $mpdf->WriteHTML($stylesheet, 1);


    $mpdf->list_indent_first_level = 0;
    $mpdf->WriteHTML($html, 2);
    $mpdf->WriteHTML($html2, 2);
    $mpdf->Output(get_template_directory() . '/tools/pdf.pdf', 'F');

    echo get_template_directory_uri() . '/tools/pdf.pdf';
}

add_action('wp_ajax_pierre_genetate_pdf', "generate_results_pdf");
add_action('wp_ajax_nopriv_pierre_genetate_pdf', "generate_results_pdf");


function pierre_date_rep($str)
{

    if (qtrans_getLanguage() == "fr") {
        $trans = array("monday" => "lundi", "tuesday" => "mardi", "wednesday" => "mercredi", "thursday" => "jeudi", "friday" => "vendredi", "saturday" => "samedi", "sunday" => "dimanche",);
        $str = strtr($str, $trans);

        $trans = array("january" => "janvier", "february" => "février", "march" => "mars", "april" => "avril", "may" => "mai",
            "june" => "juin", "july" => "juillet", "august" => "août", "september" => "septembre",
            "october" => "octobre", "november" => "novembre", "december" => "décembre",);
        $str = strtr($str, $trans);
    }

    return $str;
}

function pierre_get_background_image() {
	$what_background = get_field("what_background");
	if ($what_background == "thumbnail") {
		$size = get_post_type() == "promotions" ? 'full' : 'large';
		$thumb = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), $size );
		return $thumb['0'];
	}
	elseif ($what_background == "custompicture") {
		$what_background_picture = get_field("background_image");
		if ($what_background_picture != "") {
			return $what_background_picture;
		}
	}

	return '';
}

function pierre_slugify($text)
{
    $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

    $text = trim($text, '-');


    $text = mb_strtolower($text,"UTF-8");

    $text = preg_replace('~[^-\w]+~', '', $text);

    if (empty($text))
    {
        return 'n-a';
    }

    return $text;
}

// Enable qTranslate for WordPress SEO
function pierre_qtranslate_filter($text){
	return __($text);
}
add_filter('wpseo_title', 'pierre_qtranslate_filter', 10, 1);
add_filter('wpseo_metadesc', 'pierre_qtranslate_filter', 10, 1);
add_filter('wpseo_metakey', 'pierre_qtranslate_filter', 10, 1);


add_action('init', 'pierre_remove_feed_hooks');
function pierre_remove_feed_hooks() {
	remove_action( 'wp_head', 'feed_links', 2 );
	remove_action( 'wp_head', 'feed_links_extra', 3 );
	remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
}

add_filter('wp_headers', 'pierre_add_header');
function pierre_add_header($headers) {
    $headers['X-UA-Compatible'] = 'IE=edge,chrome=1';
    return $headers;
}
