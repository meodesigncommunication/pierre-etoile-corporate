<?php
get_header(); ?>

<div class="content-page-wrap container">
    <div class="about-head">
        <h2>404</h2>
        <p><?php _e("[:fr]rien trouvé[:en]Nothing found");?></p>
    </div>

    <div class="row content-page-article">
        <?php get_sidebar();?>
        <article class="span8 article-basic">
        </article>
    </div>
</div>

<?php
get_footer();
