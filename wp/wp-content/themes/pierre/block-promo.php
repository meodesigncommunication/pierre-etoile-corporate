<div class="promo-wrap">
    <span class="group-star">&nbsp;</span>
    <div class="container">
        <div class="promo-title">
            <h2><span><?php _e("[:en]discover[:fr]découvrez");?></span><?php _e("[:en]our current promotions[:fr]nos promotions actuelles");?></h2>
        </div>
        <div class="row">
<?php
    $args = array(
        'posts_per_page'   => 2,
        'post_type'        => 'promotions',
        'post_status'      => 'publish',
        'orderby'          => 'post_title',
        'order'            => 'ASC' );
        $args["meta_key"] = "is_active";
        $args["meta_value"] = "1";


    $promotions = get_posts($args);
    if(count($promotions)){
        foreach($promotions as $promotion){
        ?>
            <div class="span4 promo-block">
                <?php if(has_post_thumbnail($promotion->ID)){?>
                <?php echo get_the_post_thumbnail($promotion->ID, "pierreetoile-standard");?>
                <?php }else{?>
                <img src="<?php echo get_template_directory_uri();?>/images/promo1_img.png" alt="image">
                <?php }?>

                <div class="brief-info">
                    <p><?php the_field("location", $promotion->ID);?></p>
                    <h3><a href="<?php echo get_permalink($promotion->ID);?>"><?php echo get_the_title($promotion->ID);?></a></h3>
                </div>
            </div>
                <?php } }?>
            <div class="span4 newsletter-block promo-block">
                <div class="newsletter-block-inner">
                    <h3>newsletter</h3>
                    <form method="post" action="<?php echo get_permalink(PE_PAGE_ID_NEWSLETTER); ?>">
                        <input type="text" name="email-subscribe" class="email-subscribe" id="email-subscribe" placeholder="<?php _e("[:fr]votre adresse email[:en]your email address");?>" />
                        <input type="submit" value="ok" id="home-news-submit" class="subscribe-submit pull-right"/>
                    </form>
                </div>
                <div class="brief-info">
                    <p><?php _e("[:fr]en tout temps[:en]at any time");?></p>
                    <h3><a href="<?php echo get_permalink(PE_PAGE_ID_NEWSLETTER);?>"><?php _e("[:fr]restez informé[:en]stay informed");?></a></h3>
                </div>
            </div>
        </div>
    </div>
</div>