<?php
/*
* Template Name: About Us
* */
get_header();?>
<div class="about-grey-bottom-bg">
    <div class="container">
    <div class="about-head">
        <?php while(have_posts()):the_post();?>
        <h2><?php the_title();?></h2>
        <p><?php the_field("subtitle");?></p>
            <?php $team = get_field("team");
        if(is_array($team) and count($team)){
        ?>
        <ul class="unstyled inline about-us-list">
            <?php foreach($team as $i => $member){?>
            <li class="member-li">
                <input type="hidden" class="member_id" value="<?php echo $i?>">
                <?php if($member["photo"]){?>
                <img src="<?php echo $member["photo"]["sizes"]["pierreetoile-portrait"]?>" alt="<?php echo $member["name"]?>" title="<?php echo $member["name"]?>" />
                <?php }else{?>
                                <img src="<?php echo get_template_directory_uri();?>/images/unknown.png" alt="no photo" />
                <?php }?>
                                <div class="position-info">
                                    <h3><?php echo $member["name"]?></h3>
                                    <p><?php echo $member["position"]?></p>
                                </div>
                            </li>
                <?php }?>

        </ul>

        <?php
        }
        endwhile;?>
    </div>
</div>
</div>
<div class="about-us-wrapper">
    <div class="page-wrap about-us-wrap container">
        <div class="row">
            <aside class="span4">
                <div class="special">
                    <span class="single-star-grey">&nbsp;</span>
                    <?php $team = get_field("team");
                    if(is_array($team) and count($team)){
                        foreach($team as $i => $member){?>
                            <?php if(!empty($member["sidebar"])): ?>
                                <p class="member-sidebar-block member-<?php echo $i?>">
                                    <?php echo $member["sidebar"]?>
                                </p>
                            <?php endif; ?>
                            <?php }
                    }?>
                </div>
            </aside>
            <article class="span8 article-about-us">
<!--            --><?php //the_content();?>
                <?php $team = get_field("team");
                if(is_array($team) and count($team)){
                    foreach($team as $i => $member){?>
                            <div class="member-about-block member-<?php echo $i?>">
                                <?php echo $member["about"]?>
                                <br>
                                <br>
                            </div>
                        <?php }
                }?>
            </article>
        </div>
    </div>
</div>
<div class="mobile-container">
    <div class="about-us-mobile-wrap">
        <ul class="about-us-list-mobile">
            <?php $team = get_field("team");
                if(is_array($team) and count($team)){
                    foreach($team as $i => $member){?>
            <li>
                <div class="about-us-visible-item">
                    <div class="photo-wrapper">
                        <?php if($member["photo"]){?>
                            <img src="<?php echo $member["photo"]["sizes"]["pierreetoile-portrait"]?>" alt="<?php echo $member["name"]?>" title="<?php echo $member["name"]?>" />
                        <?php }else{?>
                            <img src="<?php echo get_template_directory_uri();?>/images/unknown.png" alt="no photo" />
                        <?php }?>
                    </div>
                    <div class="about-us-brief-info">
                        <div class="abouts-inner">
                            <h3><?php echo $member["name"]?></h3>
                            <p><?php echo $member["position"]?></p>
                        </div>
                        <a href="#" data-member_id="<?php echo $i;?>" class="show-info"><?php _e("[:fr]en savoir plus[:en]read more");?></a>
                        <a href="#" data-member_id="<?php echo $i;?>"  class="hide-info"><?php _e("[:fr]masquer[:en]hide");?></a>
                    </div>
                </div>
                <div class="about-us-detailed-info info-mob-<?php echo $i;?>">
                    <div class="special">
                        <span class="single-star">&nbsp;</span>
                        <p class="">
                            <?php echo $member["sidebar"]?>
                        </p>
                    </div>
                    <article class="article-about-us">

                                <div class="">
                                    <?php echo $member["about"]?>
                                    <br>
                                    <br>
                                </div>
                    </article>
                </div>
            </li>
                        <?php }
                }?>
        </ul>
    </div>
</div>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery("li.member-li").click(function(){
                var member_id = jQuery(this).children("input.member_id").val();

                jQuery(".member-about-block").slideUp("fast");
                jQuery(".member-about-block.member-"+member_id).slideDown("fast");

                jQuery(".member-sidebar-block").slideUp("fast");
                jQuery(".member-sidebar-block.member-"+member_id).slideDown("fast");

                jQuery("li.member-li").removeClass("active_member");
                jQuery(this).addClass("active_member");

            });


            jQuery(".about-us-brief-info a.show-info").click(function(){
                var member_id = jQuery(this).data("member_id");
                jQuery(".about-us-detailed-info.info-mob-"+member_id).slideDown("fast");
                jQuery(this).parent().children(".hide-info").css("display","inline-block");
                jQuery(this).hide();
                return false;
            });
            jQuery(".about-us-brief-info a.hide-info").click(function(){
                var member_id = jQuery(this).data("member_id");
                jQuery(".about-us-detailed-info.info-mob-"+member_id).slideUp("fast");
                jQuery(this).parent().children(".show-info").css("display","inline-block");
                jQuery(this).hide();
                return false;
            });

            jQuery("li.member-li:first-child").click();
        });
    </script>

<?php get_footer();?>