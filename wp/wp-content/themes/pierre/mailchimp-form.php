<!-- Begin MailChimp Signup Form -->

<!--<div id="mc_embed_signup">
    <form action="http://pierre-etoile.us3.list-manage1.com/subscribe/post?u=d1d99d3ce1d8b480ceff576cd&amp;id=c4939ce8d8" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
        <div class="mc-field-group">
            <label for="mce-FNAME"><?php _e("[:fr]Nom[:en]Name");?>  <span class="asterisk">*</span>
            </label>
            <input type="text" value="" name="FNAME" class="required" id="mce-FNAME">
        </div>
        <div class="mc-field-group">
            <label for="mce-LNAME"><?php _e("[:fr]Prénom[:en]Surname");?>  <span class="asterisk">*</span>
            </label>
            <input type="text" value="" name="LNAME" class="required" id="mce-LNAME">
        </div>
        <div class="mc-field-group">
            <label for="mce-RUE_NUMERO"><?php _e("[:fr]Rue et numéro[:en]Street and number");?> </label>
            <input type="text" value="" name="RUE_NUMERO" class="" id="mce-RUE_NUMERO">
        </div>
        <div class="mc-field-group">
            <label for="mce-POST_CODE"><?php _e("[:fr]Code postal[:en]Postal code");?> </label>
            <input type="text" value="" name="POST_CODE" class="" id="mce-POST_CODE">
        </div>
        <div class="mc-field-group">
            <label for="mce-VILLE"><?php _e("[:fr]Ville[:en]City");?> </label>
            <input type="text" value="" name="VILLE" class="" id="mce-VILLE">
        </div>
        <div class="mc-field-group">
            <label for="mce-TELEPHONE"><?php _e("[:fr]Téléphone[:en]Phone");?> </label>
            <input type="text" value="" name="TELEPHONE" class="" id="mce-TELEPHONE">
        </div>
        <div class="mc-field-group">
            <label for="mce-EMAIL">Email  <span class="asterisk">*</span>
            </label>
            <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
        </div>
        <div id="mce-responses" class="clear">
            <div class="response" id="mce-error-response" style="display:none"></div>
            <div class="response" id="mce-success-response" style="display:none"></div>
        </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
        <!--<div style="position: absolute; left: -5000px;"><input type="text" name="b_d1d99d3ce1d8b480ceff576cd_c4939ce8d8" value=""></div>
        <div class="clear"><input type="submit" value="<?php _e("[:fr]S'incrire[:en]Subscribe");?>" name="subscribe" id="mc-embedded-subscribe" class="button submit"></div>
    </form>
</div>-->
<script type="text/javascript">
var fnames = new Array();var ftypes = new Array();fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='RUE_NUMERO';ftypes[3]='text';fnames[4]='POST_CODE';ftypes[4]='text';fnames[5]='VILLE';ftypes[5]='text';fnames[6]='TELEPHONE';ftypes[6]='text';fnames[0]='EMAIL';ftypes[0]='email';
try {
    var jqueryLoaded=jQuery;
    jqueryLoaded=true;
} catch(err) {
    var jqueryLoaded=false;
}
var head= document.getElementsByTagName('head')[0];
if (!jqueryLoaded) {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = '//ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js';
    head.appendChild(script);
    if (script.readyState && script.onload!==null){
        script.onreadystatechange= function () {
            if (this.readyState == 'complete') mce_preload_check();
        }
    }
}

var err_style = '';
try{
    err_style = mc_custom_error_style;
} catch(e){
    err_style = '#mc_embed_signup input.mce_inline_error{border-color:#6B0505;} #mc_embed_signup div.mce_inline_error{margin: 0 0 1em 0; padding: 5px 10px; background-color:#6B0505; font-weight: bold; z-index: 1; color:#fff;}';
}
var head= document.getElementsByTagName('head')[0];
var style= document.createElement('style');
style.type= 'text/css';
if (style.styleSheet) {
    style.styleSheet.cssText = err_style;
} else {
    style.appendChild(document.createTextNode(err_style));
}
head.appendChild(style);
setTimeout('mce_preload_check();', 250);

var mce_preload_checks = 0;
function mce_preload_check(){
    if (mce_preload_checks>40) return;
    mce_preload_checks++;
    try {
        var jqueryLoaded=jQuery;
    } catch(err) {
        setTimeout('mce_preload_check();', 250);
        return;
    }
    try {
        var validatorLoaded=jQuery("#fake-form").validate({});
    } catch(err) {
        setTimeout('mce_preload_check();', 250);
        return;
    }
    mce_init_form();
}
function mce_init_form(){
    jQuery(document).ready( function(jQuery) {
        var options = { errorClass: 'mce_inline_error', errorElement: 'div', onkeyup: function(){}, onfocusout:function(){}, onblur:function(){}  };
        var mce_validator = jQuery("#mc-embedded-subscribe-form").validate(options);
        jQuery("#mc-embedded-subscribe-form").unbind('submit');//remove the validator so we can get into beforeSubmit on the ajaxform, which then calls the validator
        options = { url: 'http://pierre-etoile.us3.list-manage.com/subscribe/post-json?u=d1d99d3ce1d8b480ceff576cd&id=c4939ce8d8&c=?', type: 'GET', dataType: 'json', contentType: "application/json; charset=utf-8",
            beforeSubmit: function(){
                jQuery('#mce_tmp_error_msg').remove();
                jQuery('.datefield','#mc_embed_signup').each(
                        function(){
                            var txt = 'filled';
                            var fields = new Array();
                            var i = 0;
                            jQuery(':text', this).each(
                                    function(){
                                        fields[i] = this;
                                        i++;
                                    });
                            jQuery(':hidden', this).each(
                                    function(){
                                        var bday = false;
                                        if (fields.length == 2){
                                            bday = true;
                                            fields[2] = {'value':1970};//trick birthdays into having years
                                        }
                                        if ( fields[0].value=='MM' && fields[1].value=='DD' && (fields[2].value=='YYYY' || (bday && fields[2].value==1970) ) ){
                                            this.value = '';
                                        } else if ( fields[0].value=='' && fields[1].value=='' && (fields[2].value=='' || (bday && fields[2].value==1970) ) ){
                                            this.value = '';
                                        } else {
                                            if (/\[day\]/.test(fields[0].name)){
                                                this.value = fields[1].value+'/'+fields[0].value+'/'+fields[2].value;
                                            } else {
                                                this.value = fields[0].value+'/'+fields[1].value+'/'+fields[2].value;
                                            }
                                        }
                                    });
                        });
                jQuery('.phonefield-us','#mc_embed_signup').each(
                        function(){
                            var fields = new Array();
                            var i = 0;
                            jQuery(':text', this).each(
                                    function(){
                                        fields[i] = this;
                                        i++;
                                    });
                            jQuery(':hidden', this).each(
                                    function(){
                                        if ( fields[0].value.length != 3 || fields[1].value.length!=3 || fields[2].value.length!=4 ){
                                            this.value = '';
                                        } else {
                                            this.value = 'filled';
                                        }
                                    });
                        });
                return mce_validator.form();
            },
            success: mce_success_cb
        };
        jQuery('#mc-embedded-subscribe-form').ajaxForm(options);
        /*
        * Translated default messages for the jQuery validation plugin.
        * Locale: FR
        */
        <?php if(qtrans_getLanguage()=="fr"){?>
        jQuery.extend(jQuery.validator.messages, {
            required: "Ce champ est obligatoire.",
            remote: "Veuillez remplir ce champ pour continuer.",
            email: "Merci de saisir une adresse email valable.",
            url: "Veuillez entrer une URL valide.",
            date: "Veuillez entrer une date valide.",
            dateISO: "Veuillez entrer une date valide (ISO).",
            number: "Veuillez entrer un nombre valide.",
            digits: "Veuillez entrer (seulement) une valeur numérique.",
            creditcard: "Veuillez entrer un numéro de carte de crédit valide.",
            equalTo: "Veuillez entrer une nouvelle fois la même valeur.",
            accept: "Veuillez entrer une valeur avec une extension valide.",
            maxlength: jQuery.validator.format("Veuillez ne pas entrer plus de {0} caractères."),
            minlength: jQuery.validator.format("Veuillez entrer au moins {0} caractères."),
            rangelength: jQuery.validator.format("Veuillez entrer entre {0} et {1} caractères."),
            range: jQuery.validator.format("Veuillez entrer une valeur entre {0} et {1}."),
            max: jQuery.validator.format("Veuillez entrer une valeur inférieure ou égale à {0}."),
            min: jQuery.validator.format("Veuillez entrer une valeur supérieure ou égale à {0}.")
        });
    <?php }?>

    });
}
function mce_success_cb(resp){
    jQuery('#mce-success-response').hide();
    jQuery('#mce-error-response').hide();
    if (resp.result=="success"){
        jQuery('#mce-'+resp.result+'-response').show();
        <?php $message = __("[:fr]Merci pour votre inscription, vous recevrez prochainement un email de notre part[:en]Thank you for your subscription.  You'll receive an email from us shortly."); ?>
        jQuery('#mce-'+resp.result+'-response').html("<?php echo $message; ?>");
        jQuery('#mc-embedded-subscribe-form').each(function(){
            this.reset();
        });
    } else {
        var index = -1;
        var msg;
        try {
            var parts = resp.msg.split(' - ',2);
            if (parts[1]==undefined){
                msg = resp.msg;
            } else {
                i = parseInt(parts[0]);
                if (i.toString() == parts[0]){
                    index = parts[0];
                    msg = parts[1];
                } else {
                    index = -1;
                    msg = resp.msg;
                }
            }
        } catch(e){
            index = -1;
            msg = resp.msg;
        }
        try{
            if (index== -1){
                jQuery('#mce-'+resp.result+'-response').show();
                jQuery('#mce-'+resp.result+'-response').html(msg);
            } else {
                err_id = 'mce_tmp_error_msg';
                html = '<div id="'+err_id+'" style="'+err_style+'"> '+msg+'</div>';

                var input_id = '#mc_embed_signup';
                var f = jQuery(input_id);
                if (ftypes[index]=='address'){
                    input_id = '#mce-'+fnames[index]+'-addr1';
                    f = jQuery(input_id).parent().parent().get(0);
                } else if (ftypes[index]=='date'){
                    input_id = '#mce-'+fnames[index]+'-month';
                    f = jQuery(input_id).parent().parent().get(0);
                } else {
                    input_id = '#mce-'+fnames[index];
                    f = jQuery().parent(input_id).get(0);
                }
                if (f){
                    jQuery(f).append(html);
                    jQuery(input_id).focus();
                } else {
                    jQuery('#mce-'+resp.result+'-response').show();
                    jQuery('#mce-'+resp.result+'-response').html(msg);
                }
            }
        } catch(e){
            jQuery('#mce-'+resp.result+'-response').show();
            jQuery('#mce-'+resp.result+'-response').html(msg);
        }
    }
}

</script>
<!--End mc_embed_signup-->