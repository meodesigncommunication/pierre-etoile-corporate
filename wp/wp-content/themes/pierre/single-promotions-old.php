<?php
/*
* Template Name: Single Promotions
* */
get_header();?>

<?php while(have_posts()):the_post();?>
<div class="current-promo-wrap">
    <?php

//    global $_wp_additional_image_sizes;
//    print '<pre>';
////    print_r( $_wp_additional_image_sizes );
//    print_r(get_intermediate_image_sizes());
//    print '</pre>';

    if(has_post_thumbnail(get_the_ID())){?>
        <?php echo get_the_post_thumbnail(get_the_ID(), "full");?>
        <?php echo get_the_post_thumbnail(get_the_ID(), "pierreetoile-full-width");?>
        <?php echo get_the_post_thumbnail(get_the_ID(), "pierreetoile-standard");?>
    <?php }else{?>
    <img src="<?php echo get_template_directory_uri();?>/images/current_promo_img.png" alt="top image">
    <?php }?>
    <!-- <div class="container">  -->   
        
    
        <div class="promo-logo-wrap">   
        <?php
            $logo = get_field("logo");
            //print_r($logo);
            if($logo){?>

            <!--            <div class="current-logo" style="background: url('--><?php //echo $logo["url"];?><!--'); width: --><?php //echo $logo["width"];?><!--px; height: --><?php //echo $logo["height"];?><!--px; margin: --><?php
            //                if($logo["height"] > 350){ echo "0% auto -90px;";} else{
            //                    echo "18% auto 0;";
            //                }
            //                ?><!--;"></div>-->
                <div class="current-logo full-logo"  style="background: url('<?php echo $logo["url"];?>'); width: <?php echo $logo["width"];?>px; height: <?php echo $logo["height"];?>px; margin: <?php
                    if($logo["height"] > 350){ echo "0% auto -90px;";} else{
                        echo "18% auto 0;";
                    }
                    ?>;"></div>

                <div class="current-logo tablet-logo"  style="background: url('<?php echo $logo["sizes"]["large"];?>'); width: <?php echo $logo["sizes"]["large-width"];?>px; height: <?php echo $logo["sizes"]["large-height"];?>px; margin: <?php
                    if($logo["sizes"]["large-height"] > 350){ echo "0% auto -90px;";} else{
                        echo "18% auto 0;";
                    }
                    ?>;"></div>

                <div class="current-logo phone-logo"  style="background: url('<?php echo $logo["sizes"]["medium"];?>'); width: <?php echo $logo["sizes"]["medium-width"];?>px; height: <?php echo $logo["sizes"]["medium-height"];?>px; margin: <?php
                    if($logo["sizes"]["medium-height"] > 350){ echo "0% auto -90px;";} else{
                        echo "18% auto 0;";
                    }
                    ?>;"></div>
            <?php }?>
            <?php if(get_field("link")){?>
            <div class="external-link-wrap">
                <a target="_blank" href="<?php the_field("link")?>" class="external-link"><?php _e("[:fr]visitez le site[:en]visit the website");?></a>
            </div>
                <?php }?>
        </div>
        <div class="info-block-wrap">
            <div class="info-block">
                <h2><?php _e("[:fr]Infos[:en]Info");?></h2>
                <?php the_field("info");?>
            </div>
        </div>
    <!-- </div>  -->
</div>
<div class="page-wrap container">
    <div class="row">
        <?php get_sidebar();?>
        <article class="span8 article-basic promo-single">
            <?php the_content();?>
        </article>
    </div>
</div>
<?php endwhile;?>
<?php get_footer();?>