<img src="<?php

$what_background = get_field("what_background");
$what_background_picture = get_field("background_image");
if($what_background == "thumbnail"){
    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'large' );
    $thumburl = $thumb['0'];
    echo $thumburl;
}elseif(($what_background == "custompicture") and ($what_background_picture != "")){
    echo $what_background_picture;
}
elseif(is_front_page()){
    echo get_template_directory_uri()."/images/background_homepage.jpg";
}
//    }
?>" id="bg" alt="" class="bgwidth">