<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

$message_cookie = '[:fr]Afin de vous proposer le meilleur service possible, Pierre Etoile utilise des cookies. En continuant de naviguer sur le site, vous déclarez accepter leur utilisation.[:en]In order to offer you the best possible service, Pierre Etoile uses cookies. By continuing to browse this site, you declare to accept their use.[:]';
$txt_button_cookies = '[:fr]Plus d\'infos[:en]Learn more[:]';
?>



<div id="cookies-popin">
    <div class="wrapper">
        <p><?php _e($message_cookie); ?></p>
        <p class="buttons"><button onClick="hideCookiesPopin()">ok</button>
            <a href="/actualites/informations/"><?php _e($txt_button_cookies); ?></a>
        </p>
    </div>
</div>


<script>
    function showCookiesPopin() {
        if( localStorage.getItem("accept_cookies") == null ) {
            document.getElementById('cookies-popin').style.display = 'block';
        }
    }
    function hideCookiesPopin(){
        localStorage.setItem("accept_cookies",1);
        document.getElementById('cookies-popin').style.display = 'none';
    }
    window.onload = function () {
        setTimeout(function () {
            showCookiesPopin();
        }, 0);
    }
</script>

</div>
<footer class="footer-wrapper">
    <div class="container footer-inner">
        <div class="row">

            <div class="span12">
                <ul class="footer-menu">
                    <li class="contact-block-footer">
                        <div class="span2 footer-left-block">
                            <?php the_field("footer_contact_area",17);?>
                        </div>
                    </li>
                        <?php
                    $args = array(
                        'child_of'     => 9,
                        'post_type'    => 'page',
                        'post_status'  => 'publish',
                        'show_date'    => '',
                        'sort_column'  => 'menu_order, post_title',
                        'title_li'     => "<h4>".get_the_title(9)."</h4>",

                    );
                        wp_list_pages( $args );

                    $args = array(
                        'child_of'     => 11,
                        'post_type'    => 'page',
                        'post_status'  => 'publish',
                        'show_date'    => '',
                        'sort_column'  => 'menu_order, post_title',
                        'title_li'     => "<h4>".get_the_title(11)."</h4>",

                    );
                    wp_list_pages( $args );


                    $args = array(
                        'child_of'     => 13,
                        'post_type'    => 'page',
                        'post_status'  => 'publish',
                        'show_date'    => '',
                        'sort_column'  => 'menu_order, post_title',
                        'title_li'     => "<h4>".get_the_title(13)."</h4>",

                    );
                    wp_list_pages( $args );

                    $args = array(
                        'child_of'     => 15,
                        'post_type'    => 'page',
                        'post_status'  => 'publish',
                        'show_date'    => '',
                        'sort_column'  => 'menu_order, post_title',
                        'title_li'     => "<h4>".get_the_title(15)."</h4>",

                    );
                    wp_list_pages( $args );
                        ?>
                    <li class="contact-menu">
                        <h4><a href="<?php echo get_permalink(17);?>">Contact</a></h4>
                    </li>
                    <?php if(!is_front_page()){?>
                    <!--<li class="newsletter-menu">
                        <h4><a href="<?php echo get_permalink(PE_PAGE_ID_NEWSLETTER);?>"><?php echo get_the_title(PE_PAGE_ID_NEWSLETTER);?></a></h4>
                    </li>-->
                        <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</footer>
<script src="<?php echo get_template_directory_uri();?>/css/bootstrap/js/bootstrap.min.js"></script>
<script type='text/javascript'>
        jQuery(document).ready(function() {
            jQuery("#mobile-menu li.menu-item-has-children a").append("<span class='plus-minus-submenu'>&nbsp;</span>");

jQuery("#plus-media-menu").click(function(){
    if(jQuery(this).hasClass("reversed")){
        jQuery(".etape1_link, .etape2_link, .etape3_link").removeClass("opened");
        jQuery(".filter-option-list li").removeClass("opened");
        jQuery(this).removeClass("reversed");
    }
    else
    {
        jQuery(".etape1_link, .etape2_link, .etape3_link").addClass("opened");
        jQuery(".filter-option-list li").addClass("opened");
        jQuery(this).addClass("reversed");
    }
});


            var ua = navigator.userAgent.toLowerCase();
            var isAndroid = ua.indexOf("android") > -1;
            if(isAndroid) {
                jQuery("#mobile-menu .sub-menu").addClass("android-center");
            }

        });
</script>
<?php get_template_part("block","popup")?>
<?php wp_footer();?>
</body>
</html>