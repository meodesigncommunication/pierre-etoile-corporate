<?php if(!is_page(32)){ // 32 = qui nous sommes
	$sidebar_links = get_field("sidebar_links",get_the_ID());
	if (empty($sidebar_links)) {
		$sidebar_links = get_field("sidebar_links",'option');
	}

	if(is_array($sidebar_links) and count($sidebar_links)){ ?>
		<ul class="unstyled links-list"><?php
			foreach ($sidebar_links as $link) {?>
				<li><a href="<?php if($link["link_to_page"]){echo $link["page"];}else{echo $link["link_url"];}?>"<?php echo preg_match('/\/newsletter\/?$/', $link["page"]) ? '  id="get_subscribe_popup"': ''; ?>><?php echo $link["link_label"];?><span class="arrow">&nbsp;</span></a></li><?php
			} ?>
		</ul>
	<?php }else{?>
		<ul class="unstyled links-list">
			<li><a href="<?php echo get_permalink(36);?>"><?php _e("[:fr]Promotions actuelles[:en]Current promotions");?><span class="arrow">&nbsp;</span></a></li>
		</ul>
	<?php }?>
<?php }?>