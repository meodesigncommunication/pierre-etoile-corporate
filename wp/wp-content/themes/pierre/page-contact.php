<?php
/*
  * Template Name: Contact
  * */
get_header();?>
<?php ?>
<div class="container">
    <div class="row address">
        <?php the_field("main_contact_data",17);?>
    </div>
</div>
<div class="contact-wrapper">
    <div class="contact-map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2745.6827974117095!2d6.67464731559294!3d46.514388979127496!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x478c2f1d7e5ec97d%3A0x83ed70747816cd11!2sPierre+Etoile+Promotion+SA!5e0!3m2!1sfr!2sch!4v1535440105125" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
</div>
<div class="page-wrap about-us-wrap container">
    <article class="article-contact">
        <?php while(have_posts()): the_post(); the_content(); endwhile;?>
    </article>
</div>
<div class="contact-wrapper">
    <div class="container contact-form">
        <div class="row">
            <?php echo do_shortcode('[contact-form-7 id="102" title="Contact form"]');?>
        </div>
    </div>
</div>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery("label[for='surname']").html("<?php _e("[:fr]Nom*[:en]Surname*");?>");
            jQuery("label[for='first_name']").html("<?php _e("[:fr]Prénom*[:en]First name*");?>");
            jQuery("label[for='email']").html("<?php _e("[:fr]Email*[:en]email*");?>");
            jQuery("label[for='phone']").html("<?php _e("[:fr]Téléphone*[:en]Phone*");?>");
            jQuery("label[for='address']").html("<?php _e("[:fr]Adresse*[:en]Address*");?>");
            jQuery("label[for='postal_code']").html("<?php _e("[:fr]NPA*[:en]Post code*");?>");
            jQuery("label[for='city']").html("<?php _e("[:fr]Lieu*[:en]City*");?>");
            jQuery("label[for='country']").html("<?php _e("[:fr]Pays*[:en]Country*");?>");
            jQuery("label[for='message-body']").html("<?php _e("[:fr]Message*[:en]Message*");?>");
            jQuery("input.wpcf7-submit").val("<?php _e("[:fr]envoyer[:en]submit");?>");
        });
    </script>

<?php get_footer();?>
