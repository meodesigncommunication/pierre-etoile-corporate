<?php
/**
 * The Header for our theme
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri();?>/favicon.ico"/>
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<style type="text/css">
	body {
		-ms-behavior: url(<?php echo get_template_directory_uri();?>/css/backgroundsize.min.htc);
	}
	</style>
    <![endif]-->
    <?php wp_head(); ?>

    <link href='https://fonts.googleapis.com/css?family=Oswald:300,400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,400italic,700' rel='stylesheet' type='text/css'>
    <link href="<?php echo get_template_directory_uri();?>/css/style.css?<?php echo filemtime( get_stylesheet_directory() . '/css/style.css');?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri();?>/css/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <script type='text/javascript' src='<?php echo get_template_directory_uri();?>/nivo-slider/jquery.nivo.slider.js'></script>
    <script type='text/javascript' src='<?php echo get_template_directory_uri();?>/js/jquery.form-n-validate.js'></script>



    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/nivo-slider/nivo-slider.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/nivo-slider/themes/default/default.css" type="text/css" media="screen" />
    <meta name="format-detection" content="telephone=yes">
    <?php if (defined('IS_PRODUCTION') and IS_PRODUCTION) { ?>
		<script type="text/javascript">

		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-38810040-1']);
		  _gaq.push(['_setDomainName', 'pierreetoile.ch']);
		  _gaq.push(['_trackPageview']);

		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'https://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();

		</script>
    <?php } ?>
</head>

<body <?php body_class("lang-".qtrans_getLanguage()); ?>>
<header class="main-header">
    <div class="header-inner container">
        <div class="navbar">
            <?php pierre_language_selector(); ?>
            <div class="home-logo">
                <h1>
                    <a href="<?php bloginfo("url");?>">Pierre Étoile</a>
                </h1>
            </div>    
            <?php

            $defaults = array(
                'menu'            => 'MainMenu',
                'container'       => 'div',
                'container_class' => 'main-menu-wrap',
                'menu_class'      => 'main-menu',
                'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>'
            );

            wp_nav_menu( $defaults );
            ?>
            <div id="mobile-menu-container">            
            <?php
                $defaults = array(
                    'menu'            => 'MainMenu',
                    'container'       => 'div',
                    'container_id' => 'mobile-menu-wrap',
                    'container_class' => 'mobile-menu-wrap',
                    'menu_id'      => 'mobile-menu',
                    'menu_class'      => 'mobile-menu',
                    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>'
                );
                wp_nav_menu( $defaults );

                ?>
            <span id="mobile-menu-selecter" onclick="toggleMobMenu();"></span>
            </div>

            <script type="text/javascript">
                function checkMobileBrowser() {
                    var check = false;
                    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
                    return check; }
                jQuery(document).ready(function(){

                    jQuery("#menu-mainmenu-mobile").change(function(){
                        var e = document.getElementById("menu-mainmenu-mobile");
                        var val = e.options[e.selectedIndex].value;
                        window.location = val;
                    });
                    if(checkMobileBrowser()){
                        jQuery(".menu-item-has-children > a").click(function(){
                            jQuery(this).parent().children(".sub-menu").addClass("active-sub-menu");
                            return false;
                        });
                    }
                    jQuery(".menu-item-has-children").mouseout(function() {
                        jQuery(this).children(".sub-menu").removeClass("active-sub-menu");
                    });

                    var width_doc = jQuery(document).width();

                    jQuery(".has-sub-nav > a").click(function(){
                        //
                        if(jQuery(this).parent().hasClass("has-active-sub-menu")){
                            jQuery(this).parent().children(".sub-filter-nav").removeClass("active-sub-menu");
                            jQuery(this).parent().removeClass("has-active-sub-menu");
                        }
                        else{
                            jQuery(this).parent().children(".sub-filter-nav").addClass("active-sub-menu");
                            jQuery(this).parent().addClass("has-active-sub-menu");
                        }
                        return false;
                    });
                    jQuery(".has-sub-nav").mouseout(function() {
                        var is_visible = jQuery("#plus-media-menu").is(":visible");

                        if(!is_visible){
                            jQuery(this).children(".sub-filter-nav").removeClass("active-sub-menu");
                            jQuery(this).removeClass("has-active-sub-menu");
                        }

                    });


                });

                jQuery("#mobile-menu-wrap .menu-item-has-children > a").click(function(){
                    var menuhold = jQuery(this).parent();
                    var submenu = menuhold.children(".sub-menu");
                    submenu.each(function(){
                        if(jQuery(this).is(":visible")){
                            jQuery(this).slideUp("normal");
                        menuhold.removeClass("openedSubmenu");
                    }
                    else{
                            jQuery(this).slideDown("normal");
                        menuhold.addClass("openedSubmenu");
                    }
                    });
                    jQuery(this).parent().children(".sub-menu").addClass("active-sub-menu");
                    return false;
                });

                function toggleMobMenu(){
                    if(jQuery("#mobile-menu-wrap").is(':visible')){
                        jQuery("#mobile-menu-wrap").slideUp("normal");

                    }
                    else{
                        jQuery("#mobile-menu-wrap").slideDown("normal");
                    }
                    return false;
                }
            </script>
        </div>
    </div>
</header>
<?php
$image = '';
if (!is_front_page()){
	$image = pierre_get_background_image();
}
$style_attribute = ($image ? ' style="background-image:url(' . $image . '); "' : '');
?>

<div class="home-page-wrap<?php echo ($image ? ' has-background-image' : ''); ?>" <?php echo $style_attribute; ?>>
