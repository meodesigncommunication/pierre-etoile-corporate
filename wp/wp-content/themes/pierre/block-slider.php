<?php
$animation_speed = get_field("animation_speed", "option");
$pause_time = get_field("pause_time", "option");
$random_start = get_field("random_start", "option");
$effects = get_field("effects", "option");

if ($animation_speed == "") {
    $animation_speed = "500";
}
if ($pause_time == "") {
    $pause_time = "5000";
}
if ($random_start == "1") {
    $random_start = "true";
} else {
    $random_start = "false";
}
if ($effects == "") {
    $effects = "random";
}


global $post;
$slides = get_field("slides", "option");
$is_arr = false;
?>


<div class="slider-wrapper  theme-default">
    <div class="nivo-caption static-caption"><div class="slider-inner-caption"><?php the_field("main_slider_title","options");?> <span><?php the_field("main_slider_subtitle","options");?></span></div></div>
    <div class="nivoSlider" id="slider">
        <?php
        if (is_array($slides) and count($slides)) {
            foreach ($slides as $i => $slide) {
                if (is_array($slide["image"])) {
                    $img_path = $slide["image"]["url"];
                    $is_arr = true;
                } else {
                    $img_path = $slide["image"];
                }
                ?>
                <img src="<?php echo $img_path;?>"
                     data-thumb="<?php echo $img_path;?>" alt=""
                     title="#htmlcaption-<?php echo $i;?>"/>
                <?php
            }
        }
        ?>
    </div>

</div>
    <?php if ($is_arr) { ?>
    <div class="slider-wrapper  theme-default  slider-wrapper-tablet">
        <div class="nivo-caption static-caption"><?php the_field("main_slider_title","options");?> <span><?php the_field("main_slider_subtitle","options");?></span></div>
        <div class="nivoSlider for-tablet" id="slider-tablet">
            <?php
            if (is_array($slides) and count($slides)) {
                foreach ($slides as $i => $slide) {
                    $img_path = $slide["image"]["sizes"]["pierreetoile-full-width"];
                    ?>
                    <img src="<?php echo $img_path;?>"
                         data-thumb="<?php echo $img_path;?>" alt=""
                         title="#htmlcaption-t-<?php echo $i;?>"/>
                    <?php
                }
            }
            ?>
        </div>

    </div>
    <div class="slider-wrapper  theme-default slider-wrapper-phone">
        <div class="nivo-caption static-caption"><?php the_field("main_slider_title","options");?> <span><?php the_field("main_slider_subtitle","options");?></span></div>
        <div class="nivoSlider for-phone" id="slider-phone">
            <?php
            if (is_array($slides) and count($slides)) {
                foreach ($slides as $i => $slide) {
                    $img_path = $slide["image"]["sizes"]["pierreetoile-standard"];
                    ?>
                    <img src="<?php echo $img_path;?>"
                         data-thumb="<?php echo $img_path;?>" alt=""
                         title="#htmlcaption-p-<?php echo $i;?>"/>
                    <?php
                }
            }
            ?>
        </div>
    </div>
    <?php } ?>




<noscript>
    <img src="<?php echo $slide["image"]["sizes"]["pierreetoile-standard"]; ?>"
         data-thumb="<?php echo $slide["image"]["sizes"]["pierreetoile-standard"]; ?>" alt=""
         title="#htmlcaption-0"/>
</noscript>

<script type="text/javascript">
    jQuery(window).load(function () {
        jQuery('#slider').nivoSlider({
            effect:'<?php echo $effects;?>', // Specify sets like: 'fold,fade,sliceDown'
            slices:15, // For slice animations
            boxCols:8, // For box animations
            boxRows:4, // For box animations
            animSpeed:<?php echo $animation_speed;?>, // Slide transition speed
            pauseTime:<?php echo $pause_time;?>, // How long each slide will show
            startSlide:0, // Set starting Slide (0 index)
            directionNav:true, // Next & Prev navigation
            controlNav:true, // 1,2,3... navigation
            controlNavThumbs:false, // Use thumbnails for Control Nav
            pauseOnHover:true, // Stop animation while hovering
            manualAdvance:false, // Force manual transitions
            prevText:'Prev', // Prev directionNav text
            nextText:'Next', // Next directionNav text
            randomStart:<?php echo $random_start;?>, // Start on a random slide
            beforeChange:function () {
            }, // Triggers before a slide transition
            afterChange:function () {
            }, // Triggers after a slide transition
            slideshowEnd:function () {
            }, // Triggers after all slides have been shown
            lastSlide:function () {
            }, // Triggers when last slide is shown
            afterLoad:function () {
            } // Triggers when slider has loaded
        });
    <?php if ($is_arr) { ?>
        jQuery('#slider-tablet').nivoSlider({
            effect:'<?php echo $effects;?>', // Specify sets like: 'fold,fade,sliceDown'
            slices:15, // For slice animations
            boxCols:8, // For box animations
            boxRows:4, // For box animations
            animSpeed:<?php echo $animation_speed;?>, // Slide transition speed
            pauseTime:<?php echo $pause_time;?>, // How long each slide will show
            startSlide:0, // Set starting Slide (0 index)
            directionNav:true, // Next & Prev navigation
            controlNav:true, // 1,2,3... navigation
            controlNavThumbs:false, // Use thumbnails for Control Nav
            pauseOnHover:true, // Stop animation while hovering
            manualAdvance:false, // Force manual transitions
            prevText:'Prev', // Prev directionNav text
            nextText:'Next', // Next directionNav text
            randomStart:<?php echo $random_start;?>, // Start on a random slide
            beforeChange:function () {
            }, // Triggers before a slide transition
            afterChange:function () {
            }, // Triggers after a slide transition
            slideshowEnd:function () {
            }, // Triggers after all slides have been shown
            lastSlide:function () {
            }, // Triggers when last slide is shown
            afterLoad:function () {
            } // Triggers when slider has loaded
        });jQuery('#slider-phone').nivoSlider({
            effect:'<?php echo $effects;?>', // Specify sets like: 'fold,fade,sliceDown'
            slices:15, // For slice animations
            boxCols:8, // For box animations
            boxRows:4, // For box animations
            animSpeed:<?php echo $animation_speed;?>, // Slide transition speed
            pauseTime:<?php echo $pause_time;?>, // How long each slide will show
            startSlide:0, // Set starting Slide (0 index)
            directionNav:true, // Next & Prev navigation
            controlNav:true, // 1,2,3... navigation
            controlNavThumbs:false, // Use thumbnails for Control Nav
            pauseOnHover:true, // Stop animation while hovering
            manualAdvance:false, // Force manual transitions
            prevText:'Prev', // Prev directionNav text
            nextText:'Next', // Next directionNav text
            randomStart:<?php echo $random_start;?>, // Start on a random slide
            beforeChange:function () {
            }, // Triggers before a slide transition
            afterChange:function () {
            }, // Triggers after a slide transition
            slideshowEnd:function () {
            }, // Triggers after all slides have been shown
            lastSlide:function () {
            }, // Triggers when last slide is shown
            afterLoad:function () {
            } // Triggers when slider has loaded
        });
        <?php }?>
    });
</script>