<?php
/*
* Template Name: In The Media
* */
get_header();?>

<div class="container">
    <div class="about-head">
        <?php while(have_posts()):the_post();?>
        <h2><?php the_title();?></h2>
        <p><?php the_field("subtitle");?></p>
        <?php endwhile;?>
    </div>
</div>
<div class="media-filter-wrapper">
    <div class="container filter">
        <h4><?php _e("[:en]Filter[:fr]Filtrer");?><span id="plus-media-menu"></span><span id="selected-filter"><?php _e("[:en]view all[:fr]tout voir");?></span></h4>
        <ul class="unstyled inline filter-option-list">
            <li class="<?php if((!isset($_REQUEST["par-promotion"]))and(!isset($_REQUEST["par-date"]))){echo "chosen";}?>"><a href="<?php echo get_permalink()?>" class="filter-link filter-by-all"  data-filter="*"><?php _e("[:en]view all[:fr]tout voir");?></a>
                <span class="filter-param">1111</span>
            </li>
            <li class="has-sub-nav <?php if(isset($_REQUEST["par-promotion"])){echo "chosen";}?>"><a href="<?php echo pierre_add_param_to_link(get_permalink(), "par-promotion");?>" class="filter-link"><?php _e("[:en]by promotion[:fr]par promotion");?></a>
                <span class="filter-param <?php if(isset($_REQUEST["promotion_is"])){echo "filter-param-active";}?>"><?php if(isset($_REQUEST["promotion_is"])){echo $_REQUEST["promotion_is"];}else{echo "1111";}?></span>
                <?php
                global $wpdb;
                $sql = "SELECT distinct(met.`meta_value`) as val FROM `{$wpdb->postmeta}` met
LEFT JOIN `{$wpdb->posts}` pos
ON pos.`ID` = met.`post_id`
WHERE met.`meta_key` = 'promotion' AND pos.`post_type` = 'inmedia' AND pos.`post_status` = 'publish'
ORDER BY met.`meta_value` ASC";
                $results = $wpdb->get_results($sql);
                if(count($results)){
                    ?>
                    <ul class="sub-filter-nav">
                        <?php foreach($results as $result){?>
                        <li class="sub-filter-nav-item">
                            <a class="filter-by-promo" data-filter=".<?php echo pierre_slugify($result->val);?>"
                                    href="#" ><?php echo $result->val;?></a>
                        </li>
                        <?php }?>
                    </ul>
                    <?php }?>
            </li>
            <li class="has-sub-nav <?php if(isset($_REQUEST["par-date"])){echo "chosen";}?>"><a href="<?php echo pierre_add_param_to_link(get_permalink(), "par-date");?>" class="filter-link"><?php _e("[:en]by date[:fr]par date");?></a>
                <span class="filter-param <?php if(isset($_REQUEST["date_is"])){echo "filter-param-active";}?>"><?php if(isset($_REQUEST["date_is"])){echo $_REQUEST["date_is"];}else{echo "1111";}?></span>
                <?php
                global $wpdb;
                $sql = "SELECT distinct(met.`ID`) as val, met.`post_date`  FROM `{$wpdb->posts}` met
WHERE met.`post_type` = 'inmedia' AND met.`post_status` = 'publish'
ORDER BY met.`post_date` DESC";
                $results = $wpdb->get_results($sql);
                if(count($results)){
                    ?>
                    <ul class="sub-filter-nav">
                        <?php foreach($results as $result){?>
                        <li class="sub-filter-nav-item">
                            <a class="filter-by-date" data-filter=".<?php echo date("Y-m-d",strtotime($result->post_date));?>"
                               href="#"><?php echo pierre_date_rep(mb_strtolower(date("l d F Y",strtotime($result->post_date))))?></a>
                        </li>
                        <?php }?>
                    </ul>
                    <?php }?>
            </li>
        </ul>
    </div>
</div>
<div class="media-us-wrapper">
    <div class="page-wrap about-us-wrap container">
        <?php
        $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

        $args = array(
            'posts_per_page'   => 9999,
            'post_type'        => 'inmedia',
            'post_status'      => 'publish',
        'orderby'          => 'post_date',
        'order'            => 'DESC',
//            'paged' => $paged
        );

        if(isset($_REQUEST["par-date"])){
            $args["orderby"] = "post_date";
            $args["order"] = "DESC";

            if(isset($_REQUEST["date_is"])){
                $dates  = $_REQUEST["date_is"];
                $dates_arr = explode("-",$dates);
                $args["date_query"] = Array(array('year'  => $dates_arr[0],
                    'month' => $dates_arr[1],
                    'day'   => $dates_arr[2]));

            }
        }
        if(isset($_REQUEST["par-promotion"])){
//            $args["orderby"] = "meta_value";
//            $args["order"] = "ASC";
            $args["meta_key"] = "promotion";

            if(isset($_REQUEST["promotion_is"])){
                $args["meta_value"] = $_REQUEST["promotion_is"];
            }
        }

        $wp_query = new WP_Query($args);
//        echo "<pre>";
//        print_r($wp_query);
//        echo "</pre>";

//        $inmedia = get_posts($args);
//        if(count($inmedia)){
        if(have_posts()){
        ?>
        <ul id="isotop-content" class="unstyled inline media-list">
<?php //foreach($inmedia as $item){?>
<?php while(have_posts()){ the_post();global $post; $item = $post;
            $promo = get_field("promotion", $item->ID);
            ?>
            <li class="span4 item <?php echo pierre_slugify($promo);?>  <?php echo date("Y-m-d",strtotime($item->post_date));?>">
                <?php if(has_post_thumbnail($item->ID)){?>
                <?php echo get_the_post_thumbnail($item->ID, "pierreetoile-standard");?>
                <?php }else{?>
                <img src="<?php echo get_template_directory_uri();?>/images/screenshot4.png" alt="screenshot" class="sreenshot">
                        <?php }?>
                <h4 class="type-title"><?php echo $promo;?></h4>
                <h3 class="media-title"><?php echo get_the_title($item->ID);?></h3>
                <p class="publish-date"><?php _e("[:fr]Publié le[:en]Published on");?> <span><?php echo pierre_date_rep(mb_strtolower(get_the_time("l d F Y", $item->ID)))?></span></p>
                <div class="media-link-wrap">
                    <?php $content = apply_filters('the_content', get_post_field('post_content', $item->ID));
                    if(get_field("media_type", $item->ID) == "video"){ $link_text = __("[:fr]voir la vidéo[:en]view video");}
                    else{ $link_text = __("[:fr]afficher l’article[:en]view article");}
if($content){
    ?>
    <a href="#media-popup-<?php echo $item->ID;?>" class="media-link popup-open-media"><?php
        echo $link_text;
        ?></a>
    <?php }
                    else{
                    ?>
                    <a target="_blank" href="<?php the_field("media_link", $item->ID);?>" class="media-link"><?php
                        echo $link_text;
                        ?></a>
                        <?php }?>
                </div>
            </li>
            <?php }?>
        </ul>
        <div class="pagination-wrap">
            <?php //get_template_part("block","pagination");?>
            <?php }?>
        </div>
    </div>
</div>


<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<!--<script type='text/javascript' src='http://isotope.metafizzy.co/beta/isotope.pkgd.js'></script>-->
<script type='text/javascript' src='<?php echo get_template_directory_uri();?>/js/jquery.isotope.js'></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        var $container = jQuery('#isotop-content');
        $container.isotope({
            itemSelector: '.item',
            layoutMode: 'masonry'
        });



        jQuery('.filter-by-all').click(function(){
            jQuery("#plus-media-menu").removeClass("reversed");
            jQuery(".filter-option-list li").removeClass("opened");
            jQuery('.filter-option-list li').removeClass("chosen");
            jQuery(this).parent().addClass("chosen");
            var selector = jQuery(this).attr('data-filter');
            $container.isotope({ filter: selector });

            var title = jQuery(this).html();
            jQuery("#selected-filter").html(title);

            return false;
        });

        jQuery('.filter-by-promo, .filter-by-date').click(function(){
            jQuery("#plus-media-menu").removeClass("reversed");
            jQuery(".filter-option-list li").removeClass("opened");
            jQuery('.filter-option-list li').removeClass("chosen");
            jQuery(this).parent().parent().parent().addClass("chosen");

            var width_doc = jQuery(document).width();

//            jQuery(".filter-option-list li.has-sub-nav").removeClass("active-sub-menu");
//            jQuery(".filter-option-list li.has-sub-nav ul").removeClass("has-active-sub-menu");

            jQuery(this).parent().parent().removeClass("active-sub-menu");
            jQuery(this).parent().parent().parent().removeClass("has-active-sub-menu");

            if(width_doc <= 767){
//                alert(width_doc);
//                jQuery(this).parent().parent().addClass("active-sub-menu");
//                jQuery(this).parent().parent().parent().addClass("has-active-sub-menu");
            }
            var contenthtml = jQuery(this).html();
            jQuery(this).parent().parent().parent().children(".filter-param").html(contenthtml);
            jQuery(".filter-param").removeClass("filter-param-active");
            jQuery(this).parent().parent().parent().children(".filter-param").addClass("filter-param-active");
            var selector = jQuery(this).attr('data-filter');
            $container.isotope({ filter: selector });

            var title = jQuery(this).html();
            var partitle = jQuery(this).parent().parent().parent().children("a").html();
            jQuery("#selected-filter").html(partitle+":<br><span id='subfilter'>"+title+"</span>");

            return false;
        });
    });
</script>



<?php get_footer();?>


