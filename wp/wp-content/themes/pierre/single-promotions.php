<?php
/*
* Template Name: Single Promotions
* */
get_header();

while(have_posts()):the_post(); ?>

<div class="current-promo-wrap redesign-promo-wrap">

    <div class="sub-wrap">
        <?php
        $logo = get_field("logo");
        //print_r($logo);
        if($logo){?>
        <div class="logo-area">

            <img src="<?php echo $logo["sizes"]["large"];?>" alt="logo image">

        </div>
        <?php }?>
        <?php if(has_post_thumbnail(get_the_ID())){?>
        <div class="thumbnail-area">
                    <?php echo get_the_post_thumbnail(get_the_ID(), "full");?>
            <!--        --><?php //echo get_the_post_thumbnail(get_the_ID(), "pierreetoile-full-width");?>
            <!--        --><?php //echo get_the_post_thumbnail(get_the_ID(), "pierreetoile-standard");?>
        </div>
            <?php }?>
        <!-- <div class="container">  -->


        <div class="page-wrap container">
            <div class="row">
                <div class="promo-sidebar-wrap">
                    <?php get_sidebar();?>
                </div>
                <article class="span8 article-basic promo-single">
                    <?php the_content();?>
                </article>                
            </div>
        </div>

    </div>

    </div>

<?php endwhile;?>
<?php get_footer();?>