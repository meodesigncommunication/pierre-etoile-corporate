<?php
/*
  * Template Name: Develop form
  * */
get_header();?>
<div class="newsletter-wrapper">
    <div class="newsletter-description">
        <h2>Newsletter</h2>
        <p><?php _e("[:fr]Inscrivez-vous à la newsletter pour recevoir toutes les informations sur nos promotions et nos projets.[:en]Sign up for our newsletter to receive all the information about our promotions and projects.");?></p>
    </div>
    <div class="subsribe-form">
            <?php echo do_shortcode('[contact-form-7 id="746" title="Valoriser son terrain"]');?>
    </div>
</div>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery("label[for='your-name']").html("<?php _e("[:fr]Nom et prénom*[:en]Name and surname*");?>");
            jQuery("label[for='address']").html("<?php _e("[:fr]Adresse complète*[:en]Full address*");?>");
            jQuery("label[for='tel']").html("<?php _e("[:fr]Tél*[:en]Phone*");?>");
            jQuery("input.wpcf7-submit").val("<?php _e("[:fr]envoyer[:en]submit");?>");
        });
    </script>

<?php get_footer();?>
