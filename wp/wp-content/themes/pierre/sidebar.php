<?php if(get_post_type( get_the_ID() ) == "promotions"){?>

<aside class="span4">
    <?php if(get_field("link")){?>
    <div class="external-link-wrap">
        <a target="_blank" href="<?php the_field("link")?>" class="external-link"><?php _e("[:fr]visitez le site[:en]visit the website");?></a>
    </div>
    <?php }?>
    <div class="info-block-wrap">
        <div class="info-block">
            <h2><?php _e("[:fr]Infos[:en]Info");?></h2>
            <?php the_field("info");?>
        </div>
    </div>
</aside>
    <aside class="span4 sidebar_links">
<?php get_template_part("sidebar-links");?>
    </aside>
<?php }else{?>
<aside class="span4">
    <div class="special">
        <span class="single-star">&nbsp;</span>
        <p><?php
            if(get_field("sidebar_text") != ""){the_field("sidebar_text");}
            else{
            the_field("sidebar_text","option");
            }
            ?></p>
    </div>


</aside>
<aside class="span4 sidebar_links">
    <?php get_template_part("sidebar-links");?>
</aside>
<?php }?>