<div class="newsletter-wrapper">
    <div class="newsletter-description">
        <h2>Newsletter</h2>
        <p><?php _e("[:fr]Inscrivez-vous à la newsletter pour recevoir toutes les informations sur nos promotions et nos projets.[:en]Sign up for our newsletter to receive all the information about our promotions and projects.");?></p>
    </div>
    <div class="subsribe-form">
<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>-->
        <?php get_template_part("mailchimp","form")?>
    </div>
</div>