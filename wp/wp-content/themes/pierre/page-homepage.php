<?php
/*
* Template Name: Home
* */

function pierre_get_home_promotions() {
	$result = array();

	$promotions = get_field('promotions');
	foreach ($promotions as $index => $promotion) {
		$current_promotion = array();
		$promotion_id = $promotion['promotion']->ID;

		$url = get_field('link', $promotion_id);

		$current_promotion['id'] = $promotion_id;
		$current_promotion['name'] = get_the_title($promotion_id);
		$current_promotion['link'] = (!empty($url)) ? $url : get_permalink($promotion_id);
		$current_promotion['location'] = get_field('location', $promotion_id);

		$homepage_image = get_field('homepage_image', $promotion_id);
		if (!$homepage_image) {
			$promo_featured_image = wp_get_attachment_image_src( get_post_thumbnail_id($promotion_id), 'large' );
			$homepage_image = $promo_featured_image[0];
		}
		$current_promotion['homepage_image'] = $homepage_image;

		$homepage_image_offset = (int)get_field('homepage_image_offset', $promotion_id);
		if ($homepage_image_offset) {
			$current_promotion['homepage_image_offset_x'] = $homepage_image_offset . 'px';
		}
		else {
			$current_promotion['homepage_image_offset_x'] = 'center';
		}

		$homepage_image_offset_y =  (int)get_field('homepage_image_offset_y', $promotion_id);
		if ($homepage_image_offset_y) {
			$current_promotion['homepage_image_offset_y'] = $homepage_image_offset_y . 'px';
		}
		else {
			$current_promotion['homepage_image_offset_y'] = 'center';
		}

		$result[] = $current_promotion;
	}

	return $result;
}

function pierre_get_home_blocks() {
	$result = array();

	$blocks = get_field('blocks_home');
	if (!is_array($blocks)) {
		return $result;
	}

	foreach($blocks as $index => $block){
		$current_block = array();

		$current_block['title'] = $block["title"];
		$current_block['content'] = $block["content"];
		$current_block['link'] = ( $block['link_to_page'] ? $block['page'] : $block['link_url'] );

		$result[$index] = $current_block;
	}

	return $result;
}

get_header();

$image = pierre_get_background_image();
if (!$image) {
	$image = get_template_directory_uri()."/images/background_homepage.jpg";
}

$promotions = pierre_get_home_promotions();
$blocks = pierre_get_home_blocks();
?>


<div class="home-content-page-wrap" style="background-image:url(<?php echo $image; ?>); ">

	<div class="home-container">
		<div class="home-container-inner">

			<?php get_template_part("block","slider"); ?>

			<div id="content-home">
				<div class="content-home-left tablet">
					<?php foreach ($blocks as $index => $block) { ?>
						<div class="home-block creed-block" id="home-creed-block-<?php echo $index;?>">
							<?php if ($block['link']) {?><a href="<?php echo $block['link']; ?>"><?php } ?>
							<h2><?php echo $block['title']; ?></h2>
							<p><?php echo $block['content']; ?></p>
							<?php if ($block['link']) {?></a><?php } ?>
						</div>
					<?php } ?>
				</div>
				<div class="content-home-right">
					<?php foreach ($promotions as $index => $promotion) { ?>
						<div id="home-promo-block-1" class="home-block promo-block">
							<a target="_blank" href="<?php echo $promotion['link']; ?>" class="promo-home-link"  style="background: url('<?php echo $promotion['homepage_image']; ?>'); background-position: <?php echo $promotion['homepage_image_offset_x']; ?> <?php echo $promotion['homepage_image_offset_y']; ?>;"><span class="hover-text-wrap"><span class="hover-text"><?php echo $promotion['location']; ?><strong><?php echo $promotion['name']; ?></strong></span></span></a>
						</div>
					<?php } ?>
					<!--<div class="home-block newsletter-block promo-block">
						<a href="#" class="newsletter-block-inner"><span class="newsletter-text">newsletter</span></a>
					</div>-->
				</div>
				<div class="content-home-left desktop">
					<?php foreach ($blocks as $index => $block) { ?>
						<div class="home-block creed-block" id="home-creed-block-<?php echo $index;?>">
							<?php if ($block['link']) {?><a href="<?php echo $block['link']; ?>"><?php } ?>
							<h2><?php echo $block['title']; ?></h2>
							<p><?php echo $block['content']; ?></p>
							<?php if ($block['link']) {?></a><?php } ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo get_template_directory_uri();?>/js/jquery.masonry.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/css/bootstrap/js/bootstrap.min.js"></script>
<script type='text/javascript'>
	jQuery(document).ready(function() {
		jQuery('.carousel').carousel({
			interval: 1000
		})
	});
</script>
<?php get_footer();?>
