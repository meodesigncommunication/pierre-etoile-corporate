<?php
/*
* Template Name: Home
* */
get_header();?>
    <?php get_template_part("block","slider"); ?>

    <div class="container creed">
        <span class="single-star">&nbsp;</span>
        <div class="row">
            <?php while(have_posts()):the_post();?>
            <?php the_content();?>
            <?php endwhile;?>
        </div>
    </div>
    <?php get_template_part("block","promo"); ?>

<?php get_footer();?>