<div class="develop-wrapper">
    <div class="develop-description">
        <h2><?php _e("[:fr]Valoriser votre terrain à bâtir[:en]Enhance your building plot");?></h2>
        <p><?php _e("[:fr]Vous souhaitez valoriser votre terrain à bâtir? Inscrivez-vous en toute discrétion et nous vous contacterons[:en]Complete the form and we will contact to you to discuss ways to maximise the value of your plot");?>.</p>
    </div>
    <div class="subsribe-form">
            <?php echo do_shortcode('[contact-form-7 id="746" title="Valoriser son terrain"]');?>
    </div>
</div>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery("label[for='your-name']").html("<?php _e("[:fr]Nom*[:en]Name*");?>");
            jQuery("label[for='your-surname']").html("<?php _e("[:fr]Prénom*[:en]First name*");?>");
            jQuery("label[for='tel']").html("<?php _e("[:fr]Téléphone*[:en]Phone*");?>");
            jQuery("input.wpcf7-submit").val("<?php _e("[:fr]envoyer[:en]submit");?>");
        });
    </script>
