<?php if (!is_page(PE_PAGE_ID_NEWSLETTER)) { ?>
<div id="newsletter-popup">
    <div class="newsletter-popup-inner">
        <a class="newsletter-close">X</a>
        <?php get_template_part("newsletter", "wrapper")?>
    </div>
</div>
<?php wp_reset_query(); ?>
<?php if(is_page(34)){
        $args = array(
            'posts_per_page'        => -1,
            'post_type'        => 'inmedia',
            'post_status'      => 'publish',
            'orderby'          => 'post_title',
            'order'            => 'ASC',
        );

        if(isset($_REQUEST["par-date"])){
            $args["orderby"] = "post_date";
            $args["order"] = "DESC";

            if(isset($_REQUEST["date_is"])){
                $dates  = $_REQUEST["date_is"];
                $dates_arr = explode("-",$dates);
                $args["date_query"] = Array(array('year'  => $dates_arr[0],
                    'month' => $dates_arr[1],
                    'day'   => $dates_arr[2]));

            }
        }
        if(isset($_REQUEST["par-promotion"])){
            $args["orderby"] = "meta_value";
            $args["order"] = "ASC";
            $args["meta_key"] = "promotion";

            if(isset($_REQUEST["promotion_is"])){
                $args["meta_value"] = $_REQUEST["promotion_is"];
            }
        }

        $tm_query = new WP_Query($args);
        while($tm_query->have_posts()){ $tm_query->the_post();global $post; $item = $post;
        ?>
<div class="media-popup" id="media-popup-<?php echo $item->ID;?>">
    <div class="media-popup-inner">
        <a class="newsletter-close"><?php _e("[:fr]X[:en]Close");?></a>
        <div class="newsletter-wrapper newsletter-wrapper <?php if(get_field("media_type", $item->ID) == "video"){echo " video-media ";}?>">
            <?php echo apply_filters('the_content', get_post_field('post_content', $item->ID));?>
        </div>
    </div>
</div>
<?php } }?>

<div id="newsletter-overlay"></div>

<script type="text/javascript">
    function showPopupNews(){
        var top = window.pageYOffset;
        top = parseInt(top) + 50;
        jQuery("#newsletter-popup").css("top", top+"px");
        jQuery("#newsletter-popup").addClass("active");
        jQuery("#newsletter-overlay").addClass("active");
    }
    jQuery(document).ready(function () {
        jQuery("#home-news-submit").click(function () {
            var email = jQuery("#email-subscribe").val();
            jQuery("#mce-EMAIL").val(email);

            showPopupNews();
            return false;
        });

        jQuery("#get_subscribe_popup, #menu-item-49, .menu-item-49, .newsletter-menu h4 a, .newsletter-block-inner").click(function () {
            showPopupNews();
            jQuery("#mobile-menu-wrap").slideUp("normal");
            return false;
        });

        jQuery(".newsletter-close").click(function () {
            jQuery("#newsletter-popup").removeClass("active");
            jQuery(".media-popup").removeClass("active");
            jQuery("#newsletter-overlay").removeClass("active");

            jQuery('iframe').each(function(){
                jQuery(this).stopVideo();
            });

            return false;
        });



        jQuery(".popup-open-media").click(function () {
            var media_id = jQuery(this).attr("href");
            var top = window.pageYOffset;
            top = parseInt(top) + 50;
            jQuery(media_id).css("top", top+"px");
            jQuery(media_id).addClass("active");
            jQuery("#newsletter-overlay").addClass("active");
            return false;
        });


    });
</script>
<?php } ?>


<?php
/*************/
 if (is_page(45)) { ?>
<div id="develop-popup">
    <div class="develop-popup-inner">
        <a class="develop-close">X</a>
        <?php get_template_part("develop", "wrapper")?>
    </div>
</div>
<?php wp_reset_query(); ?>

<div id="develop-overlay"></div>

<script type="text/javascript">
    function showPopupDevelop(){
        var top = window.pageYOffset;
        top = parseInt(top) + 50;
        jQuery("#develop-popup").css("top", top+"px");
        jQuery("#develop-popup").addClass("active");
        jQuery("#develop-overlay").addClass("active");
    }
    
    function closePopupDevelop() {
    	jQuery("#develop-popup").removeClass("active");
        jQuery("#develop-overlay").removeClass("active");

        return false;
    }
    jQuery(document).ready(function () {

        jQuery("#develop-button").click(function () {
            showPopupDevelop();
            jQuery("#mobile-menu-wrap").slideUp("normal");
            return false;
        });

        jQuery(".develop-close").click(function () {
            closePopupDevelop();
        });


    });
</script>
<?php } ?>
