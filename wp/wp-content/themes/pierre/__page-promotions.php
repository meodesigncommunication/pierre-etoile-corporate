<?php
/*
* Template Name: All Promotions
* */
get_header();?>

<div class="page-wrap container">
    <div class="about-head">
        <?php while (have_posts()):the_post(); ?>
        <h2><?php the_title();?></h2>
        <p class="promotion-subtitle"><?php the_field("subtitle");?></p>
        <?php endwhile;?>
    </div>
    <?php
    $args = array(
        'posts_per_page' => -1,
        'post_type' => 'promotions',
        'post_status' => 'publish',
        'orderby' => 'post_date',
        'order' => 'DESC');

    if (is_page(36)) {
        $args["meta_key"] = "is_active";
        $args["meta_value"] = "1";
        $current_prom = true;
    }
    if (is_page(38)) {
        $args["meta_key"] = "is_active";
        $args["meta_value"] = "0";
        $current_prom = false;
    }


    $promotions = get_posts($args);
    if (count($promotions)) {
        ?>
        <ul class="unstyled inline promotions-past">
            <?php foreach ($promotions as $item) { ?>
            <li>
                <?php //if($current_prom){?>
                <div class="img-wrap">
                    <?php if (has_post_thumbnail($item->ID)) { ?>
                    <?php if ($current_prom) { ?>
                <a href="<?php echo get_permalink($item->ID);?>">
                    <?php } ?>
                    <?php echo get_the_post_thumbnail($item->ID, "pierreetoile-standard"); ?>
                    <?php if ($current_prom) { ?>
                </a>
                    <?php } ?>
                    <?php } else { ?>
                    <?php if ($current_prom) { ?>
                <a href="<?php echo get_permalink($item->ID);?>">
                    <?php } ?>
                    <img src="<?php echo get_template_directory_uri();?>/images/img_to_come.png" alt="screenshot"
                         class="sreenshot">
                    <?php if ($current_prom) { ?>
                </a>
                <?php } ?>
                    <?php }?>
                </div>
                <?php //}?>
                <?php if (!$current_prom) { ?>
                <span class="year"><?php the_field("years", $item->ID);?></span>
                <?php }?>
                <h3><?php if ($current_prom) { ?><a
                        href="<?php echo get_permalink($item->ID);?>"><?php echo get_the_title($item->ID);?></a><?php
                } else {
                    echo get_the_title($item->ID);
                }
                    ?></h3>

                <p><?php the_field("subtitle", $item->ID);?></p>
            </li>
            <?php }?>
            <?php if ($current_prom) { ?>
            <li>
                <div class="img-wrap">
                    <a href="<?php echo get_permalink(38);?>">
                        <img src="<?php echo get_template_directory_uri();?>/images/promotions_realizees.png"
                             alt="screenshot" class="sreenshot">
                    </a>
                </div>
                <!--            <span class="year">--><?php //_e("[:fr]Découvrez[:en]Past promotions");?><!--</span>-->
                <h3>
                    <a href="<?php echo get_permalink(38);?>"><?php _e("[:fr]Nos promotions réalisées[:en]Our past promotions");?></a>
                </h3>

                <p><?php _e("[:fr]Sur l’arc lémanique[:en]Sur l’arc lémanique");?></p>
            </li>
            <?php }?>
        </ul>
        <?php }?>
</div>

<?php get_footer(); ?>